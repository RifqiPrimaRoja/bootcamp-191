package day5;

public class NilaiKuliahNyata {

	public static void main(String[] args) {
		System.out.println("~~~~~~~~~~~~~~~~~~~~Data Nilai Kuliah Rifqi Prima Roja, S-1 Fisika FMIPA UNPAD~~~~~~~~~~~~~~~~~~~~");
		
		NilaiKuliah nl1 = new NilaiKuliah("Matematika Dasar I = A", "Kimia Dasar = B", "Praktikum Kimia Dasar = B", "Fisika Dasar I = C", "Praktikum Fisika Dasar I = B", "Biologi Umum = A", "Statistik Dasar = C", "Bahasa Inggris = B", "Kewarganegaraan = B", " ");
		System.out.println("\n");
		System.out.println("Nilai Semester 1: ");
		nl1.penunjuk();
		
		NilaiKuliah nl2 = new NilaiKuliah("Matematika Dasar II = B", "Fisika Dasar II = B", "Fisika Matematika I = B", "Algoritma dan Pemrograman = A", "Praktikum Fisika Dasar II = A", "Praktikum Algoritma dan Pemrograman = B", "Pendidikan Agama = A", "Bahasa Indonesia Ilmiah = A", "Bahasa Inggris Lanjutan = B", " ");
		System.out.println("Nilai Semester 2: ");
		nl2.penunjuk();
		
		NilaiKuliah nl3 = new NilaiKuliah("Fisika Matematika II = B", "Termodinamika = B", "Listrik Magnet I = C", "Mekanika I = A", "Elektronika I = A", "Praktikum Elektronika I = B", "Metode Numerik = A", "Praktikum Metode Numerik = A", "Fisika Eksperimen IA = B", " ");
		System.out.println("Nilai Semester 3: ");
		nl3.penunjuk();
		
		NilaiKuliah nl4 = new NilaiKuliah("Fisika Matematika III = B", "Gelombang I = A", "Mekanika II = B", "Listrik Magnet II = C", "Fisika Modern = B", "Optik = B", "Elektronika II = A", "Praktikum Elektronika II = A", "Fisika Eksperimen IB = B", "Kewirausahaan = A");
		System.out.println("Nilai Semester 4: ");
		nl4.penunjuk();
		
		NilaiKuliah nl5 = new NilaiKuliah("KKN = A", "Gelombang II = C", "Pengolahan Sinyal = C", "Fisika Statistik = A", "Fisika Ekeperimen IIA = B", "Pengantar Fisika Energi = B", "Dinamika Fluida = B", "Termodinamika Lanjutan = B", "Praktikum Fisika Energi I = B", " ");
		System.out.println("Nilai Semester 5: ");
		nl5.penunjuk();
		
		NilaiKuliah nl6 = new NilaiKuliah("Fisika Inti = B", "Fisika Zat Padat = B", "Fisika Komputas = B", "Metode Penelitian = B", "Fisika Eksperimen IIB = B", "Fisika Energi Lanjutan = B", "Pemrosesan Minyak dan Gas = A", "Praktikum Fisika Energi II = A", "Kapita Selekta Fisika Energi II = A", " ");
		System.out.println("Nilai Semester 6: ");
		nl6.penunjuk();
		
		NilaiKuliah nl7 = new NilaiKuliah("Fisika Kuantum = B", "Pemodelan = B", "Rekayasa Instalasi Nuklir = A", "Energi Baru dan Terbarukan = A", "Kapita Selekta Fisika Energi I = A", "BioFisika = B", " ", " ", " ", " ");
		System.out.println("Nilai Semester 7: ");
		nl7.penunjuk();
		
		NilaiKuliah nl8 = new NilaiKuliah("Skripsi = B", " ", " ", " ", " ", " ", " ", " ", " ", " ");
		System.out.println("Nilai Semester 8: ");
		nl8.penunjuk();
		
		System.out.println("Judul Skripsi = Pengaruh Suplai Oksigen Terhadap Laju Pembakaran Sampel Biomassa pada Kalorimeter Bomb");
		System.out.println("Total SKS = 144");
		System.out.println("GPA: 3.22");
		System.out.println("Degree Class Awarded: Very Satisfactory");
		

	}

}
