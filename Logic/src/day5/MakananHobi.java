package day5;

public class MakananHobi
{

	public static void main(String[] args) 
	{
		Makanan mkn1 = new Makanan (1, "Andi", "Jakarta", "Pria", 25, "Bakso", "Fitnes");
		System.out.println("Data Orang 1: ");
		mkn1.showData();
		
		Makanan mkn2 = new Makanan(2, "Zulfikar", "Klaten", "Pria", 24, "Pizza", "Nonton");
		System.out.println("Data Orang 2: ");
		mkn2.showData();
		
		Makanan mkn3 = new Makanan(3, "Oja", "Bekasi", "Pria", 23, "Mie Bangka", "Desain");
		System.out.println("Data Orang 3: ");
		mkn3.showData();
		
		Makanan mkn4 = new Makanan(4, "Hendra", "Bogor", "Pria", 23, "Soto Bogor", "Travel");
		System.out.println("Data Orang 4: ");
		mkn4.showData();
		
		Makanan mkn5 = new Makanan(5, "Wiwi", "Bekasi", "Wanita", 21, "Gado-gado", "Catur");
		System.out.println("Data Orang 5: ");
		mkn5.showData();
	}

}
