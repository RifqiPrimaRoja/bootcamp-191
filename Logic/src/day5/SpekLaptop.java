package day5;

public class SpekLaptop {
		
		//Identifier(penamaan) variabel dan konstanta
		//<tipe data> <nama variabel> = <nilai awal>
		String nama; //Tipe data untuk kata-kata(berupa kata/huruf)
		String prosesor;
		String GPU;
		String layar;
		String RAM;
		String HDD;
		String harga;
		
		//konstruktor
		public SpekLaptop(String nama, String prosesor, String GPU, String layar, String RAM, String HDD, String harga) {
			//this berfungsi untuk mengisi variabel
			//this ada di kelas itu sendiri, tidak bisa digunakan lagi pada class lain
			this.nama = nama; //this ini menunjukkan untuk mengisi variabel "nama"
			this.prosesor = prosesor;
			this.GPU = GPU;
			this.layar = layar;
			this.RAM = RAM;
			this.HDD = HDD;
			this.harga = harga;
		}
		
		public void tunjukData() {
			System.out.println("Nama Laptop: " +this.nama);
			System.out.println("Prosesor: " +this.prosesor);
			System.out.println("GPU : " +this.GPU);
			System.out.println("Layar : " +this.layar);
			System.out.println("RAM : " +this.RAM);
			System.out.println("HDD : " +this.HDD);
			System.out.println("Harga : " +this.harga);
			System.out.println();
		}
		

}
