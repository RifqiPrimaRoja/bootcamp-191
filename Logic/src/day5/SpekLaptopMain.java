package day5;

public class SpekLaptopMain {

	public static void main(String[] args) {
		System.out.println("~~~~~~~~~~~~~~~~~~~~Data Spesifikasi dan Harga Laptop~~~~~~~~~~~~~~~~~~~~");
		//instansiasi objek
		System.out.println("\n");
		
		SpekLaptop spk = new SpekLaptop ("Asus VivoBook S S430UN", "Intel Core i5-8250U", "Nvidia GeForce MX150", "14 inch Ultra Full HD", "8 GB RAM", "256 GB SSD", "Rp. 12.799.000,-");
		System.out.println("Spesifikasi Laptop 1: ");
		spk.tunjukData();
		
		SpekLaptop spk1 =new SpekLaptop ("Asus VivoBook S S430UN", "Intel Core i7-8550U", "Nvidia GeForce MX150", "14 inch Ultra Full HD", "8 GB RAM", "256 GB SSD", "Rp. 15.099.000,-");
		System.out.println("Spesifikasi Laptop 2: ");
		spk1.tunjukData();
		
		SpekLaptop spk2 = new SpekLaptop ("Asus X455LA", "Intel Core i3-4510U", "Integrated Intel HD Graphics 4400", "14 inch 1366 x 768", "4 GB RAM", "500 GB HDD", "Rp. 4.747.500,-");
		System.out.println("Spesifikasi Laptop 3: ");
		spk2.tunjukData();
		
		SpekLaptop spk3 = new SpekLaptop ("Asus X540 LJ", "Intel Core i5-5200U", "Nvidia GeForce 920M", "15,6 inch FHD 1920 x 1080", "4 GB RAM", "1 TB HDD", "Rp. 5.299.000,-");
		System.out.println("Spesifikasi Laptop 4: ");
		spk3.tunjukData();
		
		SpekLaptop spk4 = new SpekLaptop ("Lenovo Think T480", "Intel Core i7-8550U", "Intel HD Graphics 620", "14 inch FHD IPS", "8 GB RAM", "1 TB SSD", "Rp. 25.450.000,-");
		System.out.println("Spesifikasi Laptop 5: ");
		spk4.tunjukData();
		
		
	}

}
