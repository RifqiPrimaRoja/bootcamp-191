package day5;

public class AsalNegaraMain 
{
	
	public static void main(String[] args) 
	{
		System.out.println("~~~~~~~~~~~~~~~~~~~~\"Data Buronan Penjahat Kelas Kakap di Dunia~~~~~~~~~~~~~~~~~~~~");
		AsalNegara ngr1 = new AsalNegara ("Andi", "Indonesia", "Pria", 25, "Pimpinan PKI", "Amerika, China");
		System.out.println("Data Orang 1: ");
		ngr1.nunjukData();
	
		AsalNegara ngr2 = new AsalNegara("Zmbrucher", "Jerman", "Pria", 24, "Pimpinan Ikatan Komunitas Preman Internasional", "Jerman, Argentina");
		System.out.println("Data Orang 2: ");
		ngr2.nunjukData();
	
		AsalNegara ngr3 = new AsalNegara("Pele Zibeztan", "Brazil", "Pria", 23, "Ketua Mafia Internasional", "Indonesia, Qatar");
		System.out.println("Data Orang 3: ");
		ngr3.nunjukData();
	
		AsalNegara ngr4 = new AsalNegara("Steven Cau", "Zimbabwe", "Pria", 23, "Sindikat Narkoba", "Jepang, Turki, Finlandia");
		System.out.println("Data Orang 4: ");
		ngr4.nunjukData();
	
		AsalNegara ngr5 = new AsalNegara("Monster Hunter", "Israel", "Wanita", 21, "Pimpinan Penjajah", "Palestina, Suriah, New Zealend");
		System.out.println("Data Orang 5: ");
		ngr5.nunjukData();
	}

}
