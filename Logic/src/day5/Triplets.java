package day5;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.VoidType;

public class Triplets 
{
	static List<Integer> compareTriplets(List<Integer> a, List<Integer> b)
	{
		List<Integer> result = new ArrayList();
		
		result.add(0);
		result.add(0);
		
		int nAlice = 0;
		int nBob = 0;
		for(int i=0; i<a.size(); i++)
		{
			if(a.get(i) > b.get(i))
			{
				nAlice++;
				result.set(0,  nAlice);
			}
			if(a.get(i) < b.get(i))
			{
				nBob++;
				result.set(1,  nBob);
			}
		}
		return result;
	}
	public static void main(String[] args) throws IOException 
	{
		List<Integer> a = new ArrayList<Integer>();
		a.add(17);
		a.add(28);
		a.add(30);
		
		List<Integer> b = new ArrayList<Integer>();
		b.add(99);
		b.add(16);
		b.add(20);
		
		for(Integer item: compareTriplets(a,b))
		{
			System.out.print(item +"\t");
		}
	}
	
}
