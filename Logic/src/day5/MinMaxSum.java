package day5;

import java.util.Scanner;

public class MinMaxSum
{
	static Scanner scn;
    public static void main(String[] args) 
    {
        scn = new Scanner(System.in);
        long[] nums = new long[5];
        
        long max=0, min=0, sum=0; 
        nums[0] = max = min = sum = scn.nextLong(); 
        //Looping nilai Max dan Min
        for (int i = 1; i < 5; i++) 
        {
            nums[i] = scn.nextLong();
            if(nums[i]>max) max = nums[i];
            if(nums[i]<min) min = nums[i];
            sum += nums[i];
        }
        System.out.println((sum - max) + " " + (sum - min));  
    }
}