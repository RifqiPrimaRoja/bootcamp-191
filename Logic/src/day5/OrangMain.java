package day5;

public class OrangMain
{

	public static void main(String[] args) 
	{
		Orang org1 = new Orang (1, "Alif", "Jakarta", "Pria", 25);
		System.out.println("Data Orang 1: ");
		org1.showData();
		
		Orang org2 = new Orang(2, "Safrudin", "Klaten", "Pria", 25);
		System.out.println("Data Orang 2: ");
		org2.showData();
		
		Orang org3 = new Orang(3, "Oja", "Bekasi", "Pria", 23);
		System.out.println("Data Orang 3: ");
		org2.showData();
	}

}
