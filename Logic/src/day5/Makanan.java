package day5;

public class Makanan
{
	int id;
	String nama;
	String alamat;
	String jk;
	int umur;
	String makanan;
	String hobi;
	
// construktor
	public Makanan(int id, String nama, String alamat, String jk, int umur, String makanan, String hobi)
	{
		this.id = id;
		this.nama = nama;
		this.alamat = alamat;
		this.jk = jk;
		this.umur = umur;
		this.makanan = makanan;
		this.hobi = hobi;
	}

	public void showData() 
	{
		System.out.println("Id : "+ this.id);
		System.out.println("Nama : " + this.nama);
		System.out.println("Alamat : " + this.alamat);
		System.out.println("Jenis Kelamin : " + this.jk);
		System.out.println("Umur : " +this.umur);
		System.out.println("Makanan Kesukaan : " +this.makanan);
		System.out.println("Hobi : " +this.hobi);
		System.out.println();
	}
}