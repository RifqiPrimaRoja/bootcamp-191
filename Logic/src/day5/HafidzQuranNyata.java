package day5;

public class HafidzQuranNyata {
	
	public static void main(String[] args) 
	{
		System.out.println("~~~~~~~~~~~~~~~~~~~~\"Data Peserta Hafidz dan Hafidzah Cilik Internasional~~~~~~~~~~~~~~~~~~~~");
		HafidzQuran hfz1 = new HafidzQuran("Muhammad Thaha Al Junaydi", "Arab", "Pria", 5, 30, "Sehat");
		System.out.println("Data Orang 1: ");
		hfz1.nunjukData();
	
		HafidzQuran hfz2 = new HafidzQuran("Alzena Adzkiyya Qalsum", "China", "Wanita", 5, 20, "Sehat");
		System.out.println("Data Orang 2: ");
		hfz2.nunjukData();
	
		HafidzQuran hfz3 = new HafidzQuran("Misyari Rasyid", "Palestina", "Pria", 4, 30, "Sehat");
		System.out.println("Data Orang 3: ");
		hfz3.nunjukData();
	
		HafidzQuran hfz4 = new HafidzQuran("Wirda Mansyur", "India", "Wanita", 7, 30, "Sehat");
		System.out.println("Data Orang 4: ");
		hfz4.nunjukData();
	
		HafidzQuran hfz5 = new HafidzQuran("Jihad Al Maliki", "Arab", "Pria", 8, 30, "Buta");
		System.out.println("Data Orang 5: ");
		hfz5.nunjukData();
		
		HafidzQuran hfz6 = new HafidzQuran("Masyita", "Indonesia", "Wanita", 5, 25, "Buta");
		System.out.println("Data Orang 6: ");
		hfz6.nunjukData();
		
		HafidzQuran hfz7 = new HafidzQuran("Kayla Inara", "Arab", "Wanita", 4, 18, "Buta");
		System.out.println("Data Orang 7: ");
		hfz7.nunjukData();
		
		HafidzQuran hfz8 = new HafidzQuran("Fatih Seferagic", "Amerika", "Pria", 4, 30, "Sehat");
		System.out.println("Data Orang 8: ");
		hfz8.nunjukData();
		
		HafidzQuran hfz9 = new HafidzQuran("Idris Al Hasyimi", "Qatar", "Pria", 7, 27, "Sehat");
		System.out.println("Data Orang 9: ");
		hfz9.nunjukData();
		
		HafidzQuran hfz10 = new HafidzQuran("Ahmad Kamil", "Indonesia", "Pria", 6, 30, "Sehat");
		System.out.println("Data Orang 10: ");
		hfz10.nunjukData();
	}

}
