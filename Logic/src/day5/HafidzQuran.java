package day5;

public class HafidzQuran 
{
	String nama;
	String negara;
	String jk;
	int umur;
	int hafal;
	String fisik;
	
// construktor
	public HafidzQuran(String nama, String negara, String jk, int umur, int hafal, String fisik)
	{
		this.nama = nama;
		this.negara = negara;
		this.jk = jk;
		this.umur = umur;
		this.hafal = hafal;
		this.fisik = fisik;
	}

	public void nunjukData() 
	{
		System.out.println("Nama: " + this.nama);
		System.out.println("Asal Negara: " + this.negara);
		System.out.println("Jenis Kelamin: " + this.jk);
		System.out.println("Umur: " +this.umur+ " tahun");
		System.out.println("Jumlah Hafalan: " +this.hafal+ " Juz");
		System.out.println("Kondisi Fisik: " +this.fisik);
		System.out.println();
	}
}