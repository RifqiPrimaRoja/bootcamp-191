package day5;

public class AsalNegara
{
	String nama;
	String negara;
	String jk;
	int umur;
	String jahat;
	String sembunyi;
	
// construktor
	public AsalNegara(String nama, String negara, String jk, int umur, String jahat, String sembunyi)
	{
		this.nama = nama;
		this.negara = negara;
		this.jk = jk;
		this.umur = umur;
		this.jahat = jahat;
		this.sembunyi = sembunyi;
	}

	public void nunjukData() 
	{
		System.out.println("Nama: " + this.nama);
		System.out.println("Asal Negara: " + this.negara);
		System.out.println("Jenis Kelamin: " + this.jk);
		System.out.println("Umur: " +this.umur+ " tahun");
		System.out.println("Riwayat Kejahatan: " +this.jahat);
		System.out.println("Titik Persembunyian: " +this.sembunyi);
		System.out.println();
	}
}