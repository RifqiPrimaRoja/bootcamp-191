package day4;

import java.util.Scanner;

public class ImplementasiHr {
	{

	    /*
	     * Complete the gradingStudents function below.
	     */
	    int[] gradingStudents(int[] grades) {
	        /*
	         * Write your code here.
	         */
	        // Complete this function
	        for(int i=0;i<grades.length;i++)
	        {
	          if(grades[i]>=38)
	          {
	               if(grades[i]+(5-grades[i]%5)-grades[i]<3)
	               grades[i]=(grades[i]+(5-grades[i]%5));
	          } 
	        }
	        return grades;
	    

	    }

	    final Scanner scan = new Scanner(System.in);

	    public static void main(String[] args) throws IOException {
	        BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

	        int n = Integer.parseInt(scan.nextLine().trim());

	        int[] grades = new int[n];

	        for (int gradesItr = 0; gradesItr < n; gradesItr++) {
	            int gradesItem = Integer.parseInt(scan.nextLine().trim());
	            grades[gradesItr] = gradesItem;
	        }

	        int[] result = gradingStudents(grades);

	        for (int resultItr = 0; resultItr < result.length; resultItr++) {
	            bw.write(String.valueOf(result[resultItr]));

	            if (resultItr != result.length - 1) {
	                bw.write("\n");
	            }
	        }

	        bw.newLine();

	        bw.close();
	    }
	}
}
