package day7;

public class InterfaceMain {

	public static void main(String[] args) {
		Bike02 obj01 = new Honda02(); //Obj01 untuk Honda didalam bike
		Bike02 obj02 = new Yamaha02(); // Obj02 untuk Yamaha didalam bike
		
		Shape02 obj03 = new Circle02(); //obj03 untuk Circle didalam Shape
		Shape02 obj04 = new Rectangle02(); //obj04 untuk Rectangle didalam Shape
		Shape02 obj05 = new Oval02(); //obj05 untuk Oval didalam Shape
		
		Kampus02 obj06 = new Itb02(); //obj06 untuk ITB didalam Kampus
		Kampus02 obj07 = new Unpad02(); //obj07 untuk UNPAD didalam Kampus
		
		Wisata02 obj08 = new LeuwiHejo02(); //obj08 untuk Leuwi Hejo didalam Wisata
		Wisata02 obj09 = new SanghyangHeuleut02(); //obj09 untuk Sanghyang Heuleut didalam Wisata
		
		Negara02 obj10 = new Indonesia02(); //obj10 untuk Indonesia didalam Negara
		Negara02 obj11 = new Inggris02(); //obj11 untuk Inggris didalam Negara
		
		
		System.out.println("~~~~~Bike02~~~~~");
		obj01.run(); //memanggil method run() Honda
		obj02.run(); //memanggil method run() Yamaha
		
		obj01.component(); //memanggil method component() Honda
		obj02.component(); //memanggil method component() Yamaha
		
		obj01.fuel(); //memanggil method fuel() Honda
		obj02.fuel(); //memanggil method fuel() Yamaha
		
		obj01.speed(); //memanggil method speed() Honda
		obj02.speed(); //memanggil method speed() Yamaha
		System.out.println("\n");
		
		
		System.out.println("~~~~~Shape02~~~~~");
		obj03.draw(); //memanggil method draw() Circle
		obj04.draw(); //memanggil method draw() Rectange
		obj05.draw(); //memanggil method draw() Oval
		
		obj03.dimensi(); //memanggil method dimensi() Circle
		obj04.dimensi(); //memanggil method dimensi() Rectangle
		obj05.dimensi(); //memanggil method dimensi() Oval
		System.out.println("\n");
		
		
		System.out.println("~~~~~Kampus02~~~~~");
		obj06.nama(); //memanggil method nama() ITB
		obj06.lokasi(); //memanggil method lokasi() ITB
		
		obj07.nama(); //memanggil method nama() UNPAD
		obj07.lokasi(); //memanggil method lokasi() UNPAD
		System.out.println("\n");
		
		
		System.out.println("~~~~~Wisata02~~~~~");
		obj08.name(); //memanggil method name() Leuwi Hejo
		obj08.loc(); //memanggil method lokasi() Leuwi Hejo
		
		obj09.name(); //memanggil method nama() Sanghyang Heuleut
		obj09.loc(); //memanggil method lokasi() Sanghyang Heuleut
		System.out.println("\n");
		
		System.out.println("~~~~~Negara02~~~~~");
		obj10.country(); //memanggil method name() Leuwi Hejo
		obj10.kota(); //memanggil method lokasi() Leuwi Hejo
		
		obj11.country(); //memanggil method nama() Sanghyang Heuleut
		obj11.kota(); //memanggil method lokasi() Sanghyang Heuleut
		System.out.println("\n");
		

	}

}
