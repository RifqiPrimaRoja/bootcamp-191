package day7;

public class AbstractClass01 {

	public static void main(String[] args) {
		Bike01 obj01 = new Honda01(); //obj01 untuk Honda di dalam Bike
		Bike01 obj02 = new Yamaha01(); //obj02 untuk Yamaha di dalam Bike
			
		Shape01 obj03 = new Circle01(); //obj03 untuk Circle di dalam Shape
		Shape01 obj04 = new Rectangle01(); //obj04 untuk Rectangle di dalam Shape
		Shape01 obj05 = new Oval01(); //obj05 untuk Oval di dalam Shape
		
		Kampus01 obj06 = new Itb01(); //obj06 untuk ITB di dalam Kampus
		Kampus01 obj07 = new Unpad01(); //obj07 untuk UNPAD di dalam Kampus
		
		Wisata01 obj08 = new LeuwiHejo01(); //obj08 untuk LeuwiHejo di dalam Wisata01
		Wisata01 obj09 = new SanghyangHeuleut(); //obj09 untuk SanghyangHeuleut di dalam Wisata01
		
		Negara01 obj10 = new Indonesia01(); //obj10 untuk Indonesia di dalam Negara01
		Negara01 obj11 = new Inggris01(); //obj11 untuk Inggris di dalam Negara01
		
		System.out.println("~~~~~Bike01~~~~~");
		obj01.run(); //memanggil method run() Honda
		obj02.run(); //memanggil method run() Yamaha
		System.out.println(" ");
		obj01.component(); //memanggil method component() Honda
		obj02.component(); //memanggil method component() Yamaha
		System.out.println(" ");
		obj01.fuel(); //memanggil method fuel() Honda
		obj02.fuel(); //memanggil method fuel() Yamaha
		System.out.println(" ");
		obj01.speed(); //memanggil method speed() Honda
		obj02.speed(); //memanggil method speed() Yamaha
		System.out.println("\n");
		
		System.out.println("~~~~~Shape01~~~~~");
		obj03.draw(); //memanggil method draw() Circle
		obj04.draw(); //memanggil method draw() Rectangle
		obj05.draw(); //memanggil method draw() Oval
		System.out.println(" ");
		obj03.dimensi(); //memanggil method dimensi() Circle
		obj04.dimensi(); //memanggil method dimensi() Rectangle
		obj05.dimensi(); //memanggil method dimensi() Oval
		System.out.println("\n");
		
		System.out.println("~~~~~Kampus01~~~~~");
		obj06.nama(); //memanggil method nama() ITB
		obj07.nama(); //memanggil method nama() UNPAD
		System.out.println(" ");
		obj06.lokasi(); //memanggil method lokasi() ITB
		obj07.lokasi(); //memanggil method lokasi() UNPAD
		System.out.println("\n");
		
		System.out.println("~~~~~Wisata01~~~~~");
		obj08.name(); //memanggil method name() LeuwiHejo
		obj08.loc(); //memanggil method loc() LeuwiHejo
		System.out.println(" ");
		obj09.name(); //memanggil method name() SanghyangHeuleut
		obj09.loc(); //memanggil method loc() SanghyangHeuleut
		System.out.println("\n");
		
		System.out.println("~~~~~Negara01~~~~~");
		obj10.country(); //memanggil method country() Indonesia
		obj10.kota(); //memanggil method kota() Indonesia
		System.out.println(" ");
		obj11.country(); //memanggil method country() Inggris
		obj11.kota(); //memanggil method kota() Inggris
		
	}

}
