package day7;

public class Yamaha02 implements Bike02 {

	@Override
	public void run() {
		System.out.println("Yamaha is Running");
	}

	@Override
	public void component() {
		System.out.println("Gear Yamaha");
	}

	@Override
	public void fuel() {
		System.out.println("Bensin Yamaha");
	}

	@Override
	public int speed() {
		System.out.println("120 Km/Jam");
		return 0;
	}

}
