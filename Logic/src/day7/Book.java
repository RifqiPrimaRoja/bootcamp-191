package day7;

public abstract class Book {
	
	String title;
	abstract void setTitle(String S);
	String getTitle() {
		return title;
	}
}
