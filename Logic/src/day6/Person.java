package day6;

public class Person {
	// list properti
	public int id;
	public String name;
	public String address;
	public String gender;

	public Person(int id, String name, String address, String gender) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.gender = gender;
	}

	public Person() {

	}

}
