package day6;

public class StudentMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student std = new Student();
		std.id = 1;
		std.name = "Oja";
		std.address = "Bekasi";
		std.gender = "Laki-laki";
		std.major = "Fisika";
		std.grade = 3.22;
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~Data Peserta skripsi, dosen pembimbing, dan staff terkait~~~~~~~~~~~~~~~~~~~~");
		System.out.println("id = "+std.id);
		System.out.println("Nama =" +std.name);
		System.out.println("Alamat =" +std.address);
		System.out.println("Gender =" +std.gender);
		System.out.println("Jurusan ="+std.major);
		System.out.println("IPK =" +std.grade);
		System.out.println(" ");
		
		Dosen dsn = new Dosen("Pengaruh Suplai Oksigen Terhadap Laju Pembakaran Sampel Biomassa pada Kalorimeter Bomb",1, "Pak Otong Nurhilal, M.Si.", "Jatinangor", "Laki-laki", "Fisika Komputasi", "Fisika");
		dsn.showDosen();
		
		System.out.println(" ");
		Staff stf = new Staff(3, "Bu Sri", "Rancaekek", "Perempuan", "Staff TU");
		stf.showStaff();
	}

}
