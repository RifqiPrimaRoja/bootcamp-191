package day6;

public class Dosen extends Person {

	public String skripsi;
	public String MataKuliah;
	public String jurusan;

	//konstruktor
	public Dosen(String skripsi, int id, String name, String address, String gender, String MataKuliah, String jurusan) {
		super(id, name, address, gender);
		this.skripsi = skripsi;	
		this.MataKuliah = MataKuliah;	
		this.jurusan = jurusan;	
}

	public void showDosen() {
		System.out.println("ID :"+ super.id);
		System.out.println("Dosen pembimbing:"+super.name);
		System.out.println("Alamat:"+super.address);
		System.out.println("Gender:"+super.gender);
		System.out.println("Mata kuliah:"+this.MataKuliah);
		System.out.println("Jurusan:"+this.jurusan);
		System.out.println("Judul Skripsi:"+this.skripsi);
	}
}