package hackerrank.implementaion;

import java.util.Scanner; //Coding untuk memasukkan paket Scanner, agar mempersingkat pengetikan

public class MinMaxSum1 {

	static Scanner scan;
	public static void main(String[] args) {
		//Mendeklarasikan variabel scanner "scan"
		scan = new Scanner(System.in);
		//Memasukkan nilai variabel "number" dari Scanner yang dimasukkan dalam bentuk array
		//Array yang diinput diberi batasan 5 bilangan
		long number[] = new long [5];
		
		System.out.println("Masukkan 5 angka: "); {
		long maks = 0, min = 0, sum = 0;
		number[0] = maks = min = sum = scan.nextLong(); //Memasukkan nilai variabel "maks" = "min" = sum = array number[0]"dari Scanner dengan tipe data long
		
		//pengulangan dari i=1 sampai dengan i<5, menjadi {1,2,3,4}
		for(int i=1; i<5; i++) {
			number[i] = scan.nextLong();
			if(number[i]>maks) maks = number[i];
			if(number[i]<min) min = number[i];
			sum += number[i]; //akan menjumlahkan terus sampai 4 kali looping
		}
		System.out.println("Jumlah 4 suku pertama: " +(sum-maks)); //"sum" untuk jumlah semua bilangan dikurang "maks"(bilangan input terbesar)
		System.out.println("Jumlah 4 suku terakhir: " +(sum-min)); //"sum" untuk jumlah semua bilangan dikurang "min" (bilangan input terkecil)
		//contoh: jika bilangan input = {1,2,3,4,5}
		//1. Jumlah 4 suku pertama: sum - max = (1+2+3+4+5) - 5 = 15 - 5 = 10
		//Bukti cara lain: 4 suku pertama = U1+U2+U3+U4 = 1+2+3+4 = 10
		//2. Jumlah 4 suku terakhir: sum - min = (1+2+3+4+5) -1 = 15 - 1 = 14
		//Bukti cara lain: 4 suku terakhir = U2+U3+U4+U5 = 2+3+4+5 = 14
		}
		
		

	}

}
