package hackerrank.implementaion;

import java.util.Scanner;

public class MinMaxSum{

	static Scanner png;
	public static void main(String[] args) {
		//Mendeklarasikan variabel scanner "png"
		png = new Scanner(System.in);
		//Memasukkan nilai variabel "res" dari Scanner yang dimasukkan dalam bentuk array
		//Array yang diinput diberi batasan 5 bilangan
		long res[] = new long [5];
		
		System.out.println("Masukkan 5 angka: "); {
		long tam = 0, kur = 0, sult = 0;
		res[0] = tam = kur = sult = png.nextLong(); //Memasukkan nilai variabel "tam" = "kur" = sult = array res[0]"dari Scanner dengan tipe data long
		
		//pengulangan dari i=1 sampai dengan i<5, menjadi {1,2,3,4}
		for(int i=1; i<5; i++) {
			res[i] = png.nextLong();
			if(res[i]>tam) tam = res[i];
			if(res[i]<kur) kur = res[i];
			sult += res[i]; //akan menjumlahkan terus sampai 4 kali looping
		}
		System.out.println("Jumlah 4 suku pertama: " +(sult-tam)); //Variabel "sult" untuk jumlah semua bilangan dikurang variabel "tam"(bilangan input terbesar)
		System.out.println("Jumlah 4 suku terakhir: " +(sult-kur)); //Variabel "sult" untuk jumlah semua bilangan dikurang variabel "kur" (bilangan input terkecil)
		//contoh: jika bilangan input = {1,2,3,4,5}
		//dimisalkan khusus untuk komentar: sum(jumlah semua bilangan), max(angka terbesar), min(angka terkecil)
		//1. Jumlah 4 suku pertama: sum - max = (1+2+3+4+5) - 5 = 15 - 5 = 10
		//Bukti cara lain: 4 suku pertama = U1+U2+U3+U4 = 1+2+3+4 = 10
		//2. Jumlah 4 suku terakhir: sum - min = (1+2+3+4+5) -1 = 15 - 1 = 14
		//Bukti cara lain: 4 suku terakhir = U2+U3+U4+U5 = 2+3+4+5 = 14
		}			
	}
}
