package hackerrank.implementaion;

public class DiagonalDifference1  {
	static int diagonalDifference(int[][] ar) {
        int kanan=0; //Inisialisasi awal untuk jumlah diagonal kanan
        int kiri=0; //Inisialisasi awal untuk jumlah diagonal kiri
        for(int i=0;i<ar.length;i++){
            kanan+=ar[i][i];
            kiri+=ar[ar.length-1-i][i];
        }
        int result=Math.abs(kanan-kiri);
        return result;

    }

    public static void main(String[] args){
        int[][] ar=new int[][] {{11,2,4},{4,5,6},{10,8,-12}}; //membuat matriks 3 x 3
        int jawab=diagonalDifference(ar); //memanggil method diagonalDifference
        System.out.println("Diagonal difference : "+jawab);
    }
}
