package hackerrank.implementaion;

import java.util.Scanner;

public class MinMaxSum9 {

	static Scanner gb;
	public static void main(String[] args) {
		//Mendeklarasikan variabel scanner "gb"
		gb = new Scanner(System.in);
		//Memasukkan nilai variabel "matrix" dari Scanner yang dimasukkan dalam bentuk array
		//Array yang diinput diberi batasan 5 bilangan
		long matrix[] = new long [5];
		
		System.out.println("Masukkan 5 angka: "); {
		long many = 0, little = 0, result = 0;
		matrix[0] = many = little = result = gb.nextLong(); //Memasukkan nilai variabel "many" = "little" = result = array matrix[0]"dari Scanner dengan tipe data long
		
		//pengulangan dari i=1 sampai dengan i<5, menjadi {1,2,3,4}
		for(int i=1; i<5; i++) {
			matrix[i] = gb.nextLong();
			if(matrix[i]>many) many = matrix[i];
			if(matrix[i]<little) little = matrix[i];
			result += matrix[i]; //akan menjumlahkan terus sampai 4 kali looping
		}
		System.out.println("Jumlah 4 suku pertama: " +(result-many)); //Variabel "result" untuk jumlah semua bilangan dikurang variabel "many"(bilangan input terbesar)
		System.out.println("Jumlah 4 suku terakhir: " +(result-little)); //Variabel "result" untuk jumlah semua bilangan dikurang variabel "little" (bilangan input terkecil)
		//contoh: jika bilangan input = {1,2,3,4,5}
		//dimisalkan khusus untuk komentar: sum(jumlah semua bilangan), max(angka terbesar), min(angka terkecil)
		//1. Jumlah 4 suku pertama: sum - max = (1+2+3+4+5) - 5 = 15 - 5 = 10
		//Bukti cara lain: 4 suku pertama = U1+U2+U3+U4 = 1+2+3+4 = 10
		//2. Jumlah 4 suku terakhir: sum - min = (1+2+3+4+5) -1 = 15 - 1 = 14
		//Bukti cara lain: 4 suku terakhir = U2+U3+U4+U5 = 2+3+4+5 = 14
		}			
	}
}
