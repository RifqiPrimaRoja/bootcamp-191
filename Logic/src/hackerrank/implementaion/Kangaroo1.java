package hackerrank.implementaion;

public class Kangaroo1 {
	static String kangaroo(int x1, int v1, int x2, int v2) {
        String result="YES";
        if(x1<x2 && v1<v2){ //Apabila posisi dan laju kangguru 1 lebih unggul daripada kangguru 2 maka akan menghasilkan keluaran "NO"
            result="NO";
        } else{
            if(v1!=v2 && (x2-x1)%(v1-v2)==0){ //Apabila laju tidak sama dan percepatan=>0 maka akan menghasilkan keluaran "YES"
                result="YES";
            } else{
                result="NO";
            }
        }
        return result;
    }

    public static void main(String[] args){
    	//Posisi akhir akan menunjukkan posisi danwaktu yang bersamaan yang dilakukan oleh kedua kangguru
        int x1=0; int v1=3; //posisi & laju kangguru 1
        int x2=4; int v2=2; //posisi & laju kangguru 2
        String hasil=kangaroo(x1, v1, x2, v2);
        System.out.println(hasil);
    }
}
