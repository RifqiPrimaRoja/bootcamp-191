package hackerrank.implementaion;

public class DiagonalDifference3 {
	static int diagonalDifference(int[][] a) {
		 int right=0; //Inisialisasi awal untuk jumlah diagonal kanan
	        int left=0; //Inisialisasi awal untuk jumlah diagonal kiri
       for(int i=0;i<a.length;i++){
           right+=a[i][i];
           left+=a[a.length-1-i][i];
       }
       int result=Math.abs(right-left);
       return result;

   }

   public static void main(String[] args){
       int[][] a=new int[][] {{11,2,4},{4,5,6},{10,8,-12}}; //membuat matriks 3 x 3
       int answer=diagonalDifference(a); //memanggil method diagonalDifference
       System.out.println("Diagonal difference : "+answer);
   }
}
