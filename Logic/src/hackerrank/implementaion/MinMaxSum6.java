package hackerrank.implementaion;

import java.util.Scanner;

public class MinMaxSum6 {

	static Scanner jpg;
	public static void main(String[] args) {
		//Mendeklarasikan variabel scanner "jpg"
		jpg = new Scanner(System.in);
		//Memasukkan nilai variabel "ruang" dari Scanner yang dimasukkan dalam bentuk array
		//Array yang diinput diberi batasan 5 bilangan
		long ruang[] = new long [5];
		
		System.out.println("Masukkan 5 angka: "); {
		long ban = 0, dik = 0, jawab = 0;
		ruang[0] = ban = dik = jawab = jpg.nextLong(); //Memasukkan nilai variabel "ban" = "dik" = jawab = array ruang[0]"dari Scanner dengan tipe data long
		
		//pengulangan dari i=1 sampai dengan i<5, menjadi {1,2,3,4}
		for(int i=1; i<5; i++) {
			ruang[i] = jpg.nextLong();
			if(ruang[i]>ban) ban = ruang[i];
			if(ruang[i]<dik) dik = ruang[i];
			jawab += ruang[i]; //akan menjumlahkan terus sampai 4 kali looping
		}
		System.out.println("Jumlah 4 suku pertama: " +(jawab-ban)); //Variabel "jawab" untuk jumlah semua bilangan dikurang variabel "ban"(bilangan input terbesar)
		System.out.println("Jumlah 4 suku terakhir: " +(jawab-dik)); //Variabel "jawab" untuk jumlah semua bilangan dikurang variabel "dik" (bilangan input terkecil)
		//contoh: jika bilangan input = {1,2,3,4,5}
		//dimisalkan khusus untuk komentar: sum(jumlah semua bilangan), max(angka terbesar), min(angka terkecil)
		//1. Jumlah 4 suku pertama: sum - max = (1+2+3+4+5) - 5 = 15 - 5 = 10
		//Bukti cara lain: 4 suku pertama = U1+U2+U3+U4 = 1+2+3+4 = 10
		//2. Jumlah 4 suku terakhir: sum - min = (1+2+3+4+5) -1 = 15 - 1 = 14
		//Bukti cara lain: 4 suku terakhir = U2+U3+U4+U5 = 2+3+4+5 = 14
		}			
	}
}
