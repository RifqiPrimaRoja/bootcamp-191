package hackerrank.implementaion;

public class Kangaroo5 {
	static String kangaroo(int a, int v1, int b, int v2) {
        String result="YES";
        if(a<a && v1<v2){ //Apabila posisi dan laju kangguru 1 lebih unggul daripada kangguru 2 maka akan menghasilkan keluaran "NO"
            result="NO";
        } else{
            if(v1!=v2 && (b-a)%(v1-v2)==0){ //Apabila laju tidak sama dan percepatan=>0 maka akan menghasilkan keluaran "YES"
                result="YES";
            } else{
                result="NO";
            }
        }
        return result;
    }

    public static void main(String[] args){
    	//Posisi akhir akan menunjukkan posisi danwaktu yang bersamaan yang dilakukan oleh kedua kangguru
        int a=0; int v1=3; //posisi & laju kangguru 1
        int b=4; int v2=2; //posisi & laju kangguru 2
        String result=kangaroo(a, v1, b, v2);
        System.out.println(result);
    }
}
