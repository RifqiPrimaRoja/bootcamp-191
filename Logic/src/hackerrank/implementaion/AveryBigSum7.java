package hackerrank.implementaion;

import java.util.Scanner; //Coding untuk memasukkan paket Scanner, agar mempersingkat pengetikan

public class AveryBigSum7 {

	static Scanner nnr; //Untuk identifikasi scanner, sehingga dapat melakukan deklarasi variabel Scanner berupa variabel "nnr" dan proses pembuatan objek/input data
	public static void main(String[] args) {
		System.out.println("Input 5 bilangan: ");
		nnr = new Scanner(System.in); //Mendeklarasikan variabel scanner "nnr"
		
		//Memasukkan nilai variabel "gr" dari Scanner yang dimasukkan dalam bentuk array
		//Array yang diinput diberi batasan 5 bilangan
		long[] gr = new long[5];
		long g; //Deklarasi variabel "g" dengan tipe data "long" (untuk data yang memiliki range lebih besar dibandingkan "int"
		gr[0] = g = nnr.nextLong(); //Memasukkan nilai variabel "g" = array gr[0]"dari Scanner dengan tipe data long
		
		//Proses looping, agar setiap pengulangan dijumlahkan sampai batas yang telah ditentukan, yaitu "5 bilangan"
		//"gr.length" nilainya dianggap 5, karena sudah di inisialisasi sebelumnya yang bertuliskan "new long[5]"
		//"gr.length" bisa diinput apabila saat inisialisasi awal bertuliskan "new long[n]" dan ditambah listing program untuk input n
		//pengulangan i = 0, artinya looping dimulai dari angka 0 sampai 5 menjadi:{0,1,2,3,4,5}, totalnya jadi 6 inputan.
		//Agar totalnya menjadi 5 inputan, yaitu dengan cara mengurangi dengan angka 1 pada maksimal looping/"gr.length", menjadi:{0,1,2,3,4}
		for (int i = 0; i < gr.length-1; i++) {
			gr[i] = nnr.nextLong();
			g = g + gr[i]; //setiap looping, angka akan terus dijumlahkan
		}
		System.out.println("Hasil jumlah 5 bilangan: ");
		System.out.println(g);  //menampilkan hasil penjumlahan 5 bilangan dalam bentuk Array 1 dimensi gr[] yang sudah diubah dalam variabel "g"
	}

}
