package hackerrank.implementaion;

public class DiagonalDifference2  {
	static int diagonalDifference(int[][] mat) {
		 int kanan=0; //Inisialisasi awal untuk jumlah diagonal kanan
	        int kiri=0; //Inisialisasi awal untuk jumlah diagonal kiri
        for(int i=0;i<mat.length;i++){
            kanan+=mat[i][i];
            kiri+=mat[mat.length-1-i][i];
        }
        int result=Math.abs(kanan-kiri);
        return result;

    }

    public static void main(String[] args){
        int[][] mat=new int[][] {{11,2,4},{4,5,6},{10,8,-12}}; //membuat matriks 3 x 3
        int answer=diagonalDifference(mat); //memanggil method diagonalDifference
        System.out.println("Diagonal difference : "+answer);
    }
}
