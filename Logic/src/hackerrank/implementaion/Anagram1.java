package hackerrank.implementaion;

import java.util.HashMap;
import java.util.Map;

public class Anagram1 {

	public static void main(String[] args) {
		System.out.println(isAnagram("Makan", "Nasi"));
		System.out.println(isAnagram("Warna", "Nawar"));
		System.out.println(isAnagram("Sakit", "Sikat"));
		System.out.println(isAnagram("Nasi", "asin"));
		System.out.println(isAnagram("Tikus", "Sikut"));
	}

	public static boolean isAnagram(String c, String d) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		String[] c1 = c.toLowerCase().split("");
		String[] d1 = d.toLowerCase().split("");
		for (int i = 0; i < c1.length; i++) {
			if(map.containsKey(c1[i])) {
				int n = map.get(c1[i]);
				n++;
				map.put(c1[i], n);
			}
			else {
				map.put(c1[i], 1);
			}
		}
		
		for (int i = 0; i < d1.length; i++) {
			if(!map.containsKey(d1[i])) {
				return false;
			}
			int n = map.get(d1[i]);
			if(n==0) {
				return false;
			}
			else {
				n--;
				map.put(d1[i], n);
			}
			
		}
		return true;
			
		}
}
