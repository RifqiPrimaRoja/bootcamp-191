package hackerrank.implementaion;

import java.util.Scanner;

public class SimplaArraySum5 {

	static Scanner copy;
	public static void main(String[] args) {
		copy = new Scanner(System.in);
		int math[] = new int[5]; //batasan array sampai i=5
		int x =0;
		System.out.println("Masukkan 6 angka: "); 
		math[0] = x = copy.nextInt();
		
		//Pengulangan array dimulai dari i=0 sampai i=5, berarti menjadi: {0,1,2,3,4,5}, totalnya ad 6 bilangan yang dapat dijumlahkan
		//Agar sesuai permintaan di Hackerrank, maka input angka :{1,2,3,4,10,11}
		//Dengan looping dibawah maka dieksekusi seperti: 1+2+3+4+10+11 = 31 (Sesuai dari angka inputan)
		for (int i = 0; i < math.length; i++) {
			math[i] = copy.nextInt();
			x+= math[i];
		}
		System.out.println("Hasil Penjumlahan: "); //Keterangan untuk menampilkan hasil penjumlahan
		System.out.println(x); //menampilkan hasil penjumlahan Array 1 dimensi math[] yang sudah diubah dalam variabel "x"
	}
}