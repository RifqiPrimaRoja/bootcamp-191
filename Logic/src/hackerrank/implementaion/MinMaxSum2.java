package hackerrank.implementaion;

import java.util.Scanner;

public class MinMaxSum2 {

	static Scanner sn;
	public static void main(String[] args) {
		//Mendeklarasikan variabel scanner "sn"
		sn = new Scanner(System.in);
		//Memasukkan nilai variabel "nomor" dari Scanner yang dimasukkan dalam bentuk array
		//Array yang diinput diberi batasan 5 bilangan
		long nomor[] = new long [5];
		
		System.out.println("Masukkan 5 angka: "); {
		long maksimal = 0, minimal = 0, jumlah = 0;
		nomor[0] = maksimal = minimal = jumlah = sn.nextLong(); //Memasukkan nilai variabel "maksimal" = "minimal" = jumlah = array nomor[0]"dari Scanner dengan tipe data long
		
		//pengulangan dari i=1 sampai dengan i<5, menjadi {1,2,3,4}
		for(int i=1; i<5; i++) {
			nomor[i] = sn.nextLong();
			if(nomor[i]>maksimal) maksimal = nomor[i];
			if(nomor[i]<minimal) minimal = nomor[i];
			jumlah += nomor[i]; //akan menjumlahkan terus sampai 4 kali looping
		}
		System.out.println("Jumlah 4 suku pertama: " +(jumlah-maksimal)); //Variabel "jumlah" untuk jumlah semua bilangan dikurang variabel "maksimal"(bilangan input terbesar)
		System.out.println("Jumlah 4 suku terakhir: " +(jumlah-minimal)); //Variabel "jumlah" untuk jumlah semua bilangan dikurang variabel "minimal" (bilangan input terkecil)
		//contoh: jika bilangan input = {1,2,3,4,5}
		//1. Jumlah 4 suku pertama: sum - max = (1+2+3+4+5) - 5 = 15 - 5 = 10
		//Bukti cara lain: 4 suku pertama = U1+U2+U3+U4 = 1+2+3+4 = 10
		//2. Jumlah 4 suku terakhir: sum - min = (1+2+3+4+5) -1 = 15 - 1 = 14
		//Bukti cara lain: 4 suku terakhir = U2+U3+U4+U5 = 2+3+4+5 = 14
		}			
	}
}
