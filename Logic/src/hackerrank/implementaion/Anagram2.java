package hackerrank.implementaion;

import java.util.HashMap;
import java.util.Map;

public class Anagram2 {

	public static void main(String[] args) {
		System.out.println(isAnagram("Sayur", "Asem"));
		System.out.println(isAnagram("Gula", "Lagu"));
		System.out.println(isAnagram("Rakus", "Kuras"));
		System.out.println(isAnagram("Kasur", "Rusak"));
		System.out.println(isAnagram("Satu", "Tuas"));
	}

	public static boolean isAnagram(String e, String f) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		String[] e1 = e.toLowerCase().split("");
		String[] f1 = f.toLowerCase().split("");
		for (int i = 0; i < e1.length; i++) {
			if(map.containsKey(e1[i])) {
				int n = map.get(e1[i]);
				n++;
				map.put(e1[i], n);
			}
			else {
				map.put(e1[i], 1);
			}
		}
		
		for (int i = 0; i < f1.length; i++) {
			if(!map.containsKey(f1[i])) {
				return false;
			}
			int n = map.get(f1[i]);
			if(n==0) {
				return false;
			}
			else {
				n--;
				map.put(f1[i], n);
			}
			
		}
		return true;
			
		}
}
