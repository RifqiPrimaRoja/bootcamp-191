package hackerrank.implementaion;

public class AveryBigClass {
	//Fungsi Static: bisa langsung di eksekusi, tanpa perlu membuat instansiasi objek
	static long aVeryBigSum(long[] ar) {
		long a = 0;
		for (int i = 0; i < ar.length; i++) {
			a+= ar[i];
			
		}
		//mengembalikan nilai setelah fungsi memproses data yang diinputkan melalui parameter
		//Nilai hasil kembalian, kemudian diolah pada proses berikutnya
		return a; 
	//Dari listing program diatas, parameter "ar" dibuat, kemudian fungsi mengembalikan nilai dengan tipe data "long" dari variabel a.
	}
	
	//Fungsi main()
	public static void main(String[] args) {
		System.out.println("Masukkan 5 bilangan yang ingin dijumlahkan: ");
		long[] ar = { 1, 2, 3, 4, 5 };
		long a = aVeryBigSum(ar);
		System.out.println("Hasil Penjumlahan: ");
		System.out.println(a);
		
	}
	

}
