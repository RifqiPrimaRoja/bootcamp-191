package hackerrank.implementaion;

import java.util.Scanner;

public class MinMaxSum8 {

	static Scanner gmb;
	public static void main(String[] args) {
		//Mendeklarasikan variabel scanner "gmb"
		gmb = new Scanner(System.in);
		//Memasukkan nilai variabel "rr" dari Scanner yang dimasukkan dalam bentuk array
		//Array yang diinput diberi batasan 5 bilangan
		long rr[] = new long [5];
		
		System.out.println("Masukkan 5 angka: "); {
		long ny = 0, ik = 0, ul = 0;
		rr[0] = ny = ik = ul = gmb.nextLong(); //Memasukkan nilai variabel "ny" = "ik" = ul = array rr[0]"dari Scanner dengan tipe data long
		
		//pengulangan dari i=1 sampai dengan i<5, menjadi {1,2,3,4}
		for(int i=1; i<5; i++) {
			rr[i] = gmb.nextLong();
			if(rr[i]>ny) ny = rr[i];
			if(rr[i]<ik) ik = rr[i];
			ul = ul + rr[i]; //akan menjumlahkan terus sampai 4 kali looping
		}
		System.out.println("Jumlah 4 suku pertama: " +(ul-ny)); //Variabel "ul" untuk jumlah semua bilangan dikurang variabel "ny"(bilangan input terbesar)
		System.out.println("Jumlah 4 suku terakhir: " +(ul-ik)); //Variabel "ul" untuk jumlah semua bilangan dikurang variabel "ik" (bilangan input terkecil)
		//contoh: jika bilangan input = {1,2,3,4,5}
		//dimisalkan khusus untuk komentar: sum(jumlah semua bilangan), max(angka terbesar), min(angka terkecil)
		//1. Jumlah 4 suku pertama: sum - max = (1+2+3+4+5) - 5 = 15 - 5 = 10
		//Bukti cara lain: 4 suku pertama = U1+U2+U3+U4 = 1+2+3+4 = 10
		//2. Jumlah 4 suku terakhir: sum - min = (1+2+3+4+5) -1 = 15 - 1 = 14
		//Bukti cara lain: 4 suku terakhir = U2+U3+U4+U5 = 2+3+4+5 = 14
		}			
	}
}
