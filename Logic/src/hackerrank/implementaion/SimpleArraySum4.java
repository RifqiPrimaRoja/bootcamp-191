package hackerrank.implementaion;

import java.util.Scanner;

public class SimpleArraySum4 {

	static Scanner cn;
	public static void main(String[] args) {
		cn = new Scanner(System.in);
		int arr[] = new int[5]; //batasan array sampai i=5
		int c =0;
		System.out.println("Masukkan 6 angka: "); 
		arr[0] = c = cn.nextInt();
		
		//Pengulangan array dimulai dari i=0 sampai i=5, berarti menjadi: {0,1,2,3,4,5}, totalnya ad 6 bilangan yang dapat dijumlahkan
		//Agar sesuai permintaan di Hackerrank, maka input angka :{1,2,3,4,10,11}
		//Dengan looping dibawah maka dieksekusi seperti: 1+2+3+4+10+11 = 31 (Sesuai dari angka inputan)
		for (int i = 0; i < arr.length; i++) {
			arr[i] = cn.nextInt();
			c+= arr[i];
		}
		System.out.println("Hasil Penjumlahan: "); //Keterangan untuk menampilkan hasil penjumlahan
		System.out.println(c); //menampilkan hasil penjumlahan Array 1 dimensi arr[] yang sudah diubah dalam variabel "c"
		

	}
}