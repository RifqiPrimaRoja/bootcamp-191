package hackerrank.implementaion;

public class TheHurdleRace2  {

	public static int hurdleRace (int l, int[] tinggi) {
		int maks = tinggi[0];
		
		for (int i = 1; i < tinggi.length; i++) {
			if(tinggi[i] > maks) {
				maks = tinggi[i];
			}
		}
		if(l < maks) {
			return maks - l;
		}
		else {
			return 0;
		}
	}
	
	
	public static void main(String[] args) {
		System.out.println(hurdleRace(4, new int[] {1,6,3,5,2}));
	}

}