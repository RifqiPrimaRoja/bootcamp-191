package hackerrank.implementaion;

import java.util.Scanner; //Coding untuk memasukkan paket Scanner, agar mempersingkat pengetikan

public class AveryBigSum5 {

	static Scanner copy; //Untuk identifikasi scanner, sehingga dapat melakukan deklarasi variabel Scanner berupa variabel "copy" dan proses pembuatan objek/input data
	
	//Fungsi main()
	public static void main(String[] args) {
		System.out.println("Input 5 bilangan: "); //Keterangan, untuk input 5 bilangan yang ingin dijumlahkan
		//Mendeklarasikan variabel scanner "copy"
		copy = new Scanner(System.in);
		//Memasukkan nilai variabel "er" dari Scanner yang dimasukkan dalam bentuk array
		//Array yang diinput diberi batasan 5 bilangan
		long[] er = new long[5];
		
		//inisialisasi awal dengan nilai e mulai dari 0
		//Deklarasi variabel "e" dengan tipe data "long" (untuk data yang memiliki range lebih besar dibandingkan "int"
		//long memiliki ukuran 64 bit, lebih besar dibandingkan int yang hanya 32 bit
		long e = 0;
		//Memasukkan nilai variabel "e" = array er[0]"dari Scanner dengan tipe data long
		er[0] = e = copy.nextLong();
		
		//Proses looping, agar setiap pengulangan dijumlahkan sampai batas yang telah ditentukan, yaitu "5 bilangan"
		//"er.length" nilainya dianggap 5, karena sudah di inisialisasi sebelumnya yang bertuliskan "new long[5]"
		//"er.length" bisa diinput apabila saat inisialisasi awal bertuliskan "new long[n]" dan ditambah listing program untuk input n
		//pengulangan i = 1, artinya looping dimulai dari angka 1 sampai 5, karena jika dimulai dari 0, loopingnya menjadi dari 0, 1, 2, 3, 4, 5, maka totalnya menjadi 6 bilangan yang diinput.
		for (int i = 1; i < er.length; i++) {
			er[i] = copy.nextLong();
					e+= er[i];
		}
		
	System.out.println("Hasil jumlah 5 bilangan: ");	//Keterangan, untuk menampilkan hasil penjumlahan
	System.out.println(e); //menampilkan hasil penjumlahan Array 1 dimensi er[] yang sudah diubah dalam variabel "e"
	}

}
