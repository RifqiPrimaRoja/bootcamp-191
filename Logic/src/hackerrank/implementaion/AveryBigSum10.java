package hackerrank.implementaion;

public class AveryBigSum10 {
	//Fungsi Static: bisa langsung di eksekusi, tanpa perlu membuat instansiasi objek
	static long aVeryBigSum(long[] mn) {
		long m = 0;
		for (int i = 0; i < mn.length; i++) {
			m+= mn[i];
			
		}
		//mengembalikan nilai setelah fungsi memproses data yang diinputkan melalui parameter
		//Nilai hasil kembalian, kemudian diolah pada proses berikutnya
		return m; 
	//Dari listing program diatas, parameter "mn" dibuat, kemudian fungsi mengembalikan nilai dengan tipe data "long" dari variabel m.
	}
	
	//Fungsi main()
	public static void main(String[] args) {
		//Untuk listing program yang ke 10 ini dengan cara lain yaitu tanpa inputan, melainkan angka sudah diinput didalam listing program
		//Pada tampilan, langsung berupa hasil
		long[] mn = { 1000000001, 1000000002, 1000000003, 1000000004, 1000000005 };
		long m = aVeryBigSum(mn); //pemanggilan fungsi static
		System.out.println("Hasil Penjumlahan 5 bilangan: ");
		System.out.print("1000000001 + 1000000002 + 1000000003 + 1000000004 + 100000005 = ");
		System.out.println(m);
		
	}
	

}