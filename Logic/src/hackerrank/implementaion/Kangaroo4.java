package hackerrank.implementaion;

public class Kangaroo4 {
	static String kangaroo(int y1, int v1, int y2, int v2) {
        String result="YES";
        if(y1<y2 && v1<v2){ //Apabila posisi dan laju kangguru 1 lebih unggul daripada kangguru 2 maka akan menghasilkan keluaran "NO"
            result="NO";
        } else{
            if(v1!=v2 && (y2-y1)%(v1-v2)==0){ //Apabila laju tidak sama dan percepatan=>0 maka akan menghasilkan keluaran "YES"
                result="YES";
            } else{
                result="NO";
            }
        }
        return result;
    }

    public static void main(String[] args){
    	//Posisi akhir akan menunjukkan posisi danwaktu yang bersamaan yang dilakukan oleh kedua kangguru
        int y1=0; int v1=3; //posisi & laju kangguru 1
        int y2=4; int v2=2; //posisi & laju kangguru 2
        String hasil=kangaroo(y1, v1, y2, v2);
        System.out.println(hasil);
    }
}
