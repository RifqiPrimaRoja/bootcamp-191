package hackerrank.implementaion;

import java.util.Scanner; //Coding untuk memasukkan paket Scanner, agar mempersingkat pengetikan

public class AveryBigSum9 {

	static Scanner hasil; //Untuk identifikasi scanner, sehingga dapat melakukan deklarasi variabel Scanner berupa variabel "hasil" dan proses pembuatan objek/input data
	public static void main(String[] args) {
		System.out.println("Input 5 bilangan: ");
		hasil = new Scanner(System.in);
		
		long[] jk = new long[5];
		long j;
		jk[0] = j = hasil.nextLong(); //Memasukkan nilai variabel "j" = array jk[0]"dari Scanner dengan tipe data long
		
		//Proses looping, agar setiap pengulangan dijumlahkan sampai batas yang telah ditentukan, yaitu "5 bilangan"
		//Agar totalnya menjadi 5 inputan, yaitu dengan cara mengurangi dengan angka 1 pada maksimal looping/"jk.length", menjadi:{0,1,2,3,4}
		for (int i = 0; i < jk.length-1; i++) {
			jk[i] = hasil.nextLong();
			j = j + jk[i];
		}
		System.out.println("Hasil jumlah kelima bilangan: ");
		System.out.println(j);

	}

}
