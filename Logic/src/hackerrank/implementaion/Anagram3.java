package hackerrank.implementaion;

import java.util.HashMap;
import java.util.Map;

public class Anagram3 {

	public static void main(String[] args) {
		System.out.println(isAnagram("Sambal", "Terasi"));
		System.out.println(isAnagram("Sambal", "Balsam"));
		System.out.println(isAnagram("Tapi", "Pita"));
		System.out.println(isAnagram("Kamar", "Makar"));
		System.out.println(isAnagram("Palu", "Pula"));
	}

	public static boolean isAnagram(String x, String y) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		String[] x1 = x.toLowerCase().split("");
		String[] y1 = y.toLowerCase().split("");
		for (int i = 0; i < x1.length; i++) {
			if(map.containsKey(x1[i])) {
				int n = map.get(x1[i]);
				n++;
				map.put(x1[i], n);
			}
			else {
				map.put(x1[i], 1);
			}
		}
		
		for (int i = 0; i < y1.length; i++) {
			if(!map.containsKey(y1[i])) {
				return false;
			}
			int n = map.get(y1[i]);
			if(n==0) {
				return false;
			}
			else {
				n--;
				map.put(y1[i], n);
			}
			
		}
		return true;
			
		}
}
