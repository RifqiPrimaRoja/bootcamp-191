package hackerrank.implementaion;

import java.util.Scanner;

public class MinMaxSum10 {

	static Scanner optik;
	public static void main(String[] args) {
		//Mendeklarasikan variabel scanner "optik"
		optik = new Scanner(System.in);
		//Memasukkan nilai variabel "metnum" dari Scanner yang dimasukkan dalam bentuk array
		//Array yang diinput diberi batasan 5 bilangan
		long metnum[] = new long [5];
		
		System.out.println("Masukkan 5 angka: "); {
		long plus = 0, minus = 0, akhir = 0;
		metnum[0] = plus = minus = akhir = optik.nextLong(); //Memasukkan nilai variabel "plus" = "minus" = akhir = array metnum[0]"dari Scanner dengan tipe data long
		
		//pengulangan dari i=1 sampai dengan i<5, menjadi {1,2,3,4}
		for(int i=1; i<5; i++) {
			metnum[i] = optik.nextLong();
			if(metnum[i]>plus) plus = metnum[i];
			if(metnum[i]<minus) minus = metnum[i];
			akhir += metnum[i]; //akan menjumlahkan terus sampai 4 kali looping
		}
		System.out.println("Jumlah 4 suku pertama: " +(akhir-plus)); //Variabel "akhir" untuk jumlah semua bilangan dikurang variabel "plus"(bilangan input terbesar)
		System.out.println("Jumlah 4 suku terakhir: " +(akhir-minus)); //Variabel "akhir" untuk jumlah semua bilangan dikurang variabel "minus" (bilangan input terkecil)
		//contoh: jika bilangan input = {1,2,3,4,5}
		//dimisalkan khusus untuk komentar: sum(jumlah semua bilangan), max(angka terbesar), min(angka terkecil)
		//1. Jumlah 4 suku pertama: sum - max = (1+2+3+4+5) - 5 = 15 - 5 = 10
		//Bukti cara lain: 4 suku pertama = U1+U2+U3+U4 = 1+2+3+4 = 10
		//2. Jumlah 4 suku terakhir: sum - min = (1+2+3+4+5) -1 = 15 - 1 = 14
		//Bukti cara lain: 4 suku terakhir = U2+U3+U4+U5 = 2+3+4+5 = 14
		}			
	}
}
