package hackerrank.implementaion;

import java.util.Scanner;

public class MinMaxSum7 {

	static Scanner img;
	public static void main(String[] args) {
		//Mendeklarasikan variabel scanner "img"
		img = new Scanner(System.in);
		//Memasukkan nilai variabel "ray" dari Scanner yang dimasukkan dalam bentuk array
		//Array yang diinput diberi batasan 5 bilangan
		long ray[] = new long [5];
		
		System.out.println("Masukkan 5 angka: "); {
		long ak = 0, it = 0, esu = 0;
		ray[0] = ak = it = esu = img.nextLong(); //Memasukkan nilai variabel "ak" = "it" = esu = array ray[0]"dari Scanner dengan tipe data long
		
		//pengulangan dari i=1 sampai dengan i<5, menjadi {1,2,3,4}
		for(int i=1; i<5; i++) {
			ray[i] = img.nextLong();
			if(ray[i]>ak) ak = ray[i];
			if(ray[i]<it) it = ray[i];
			esu += ray[i]; //akan menjumlahkan terus sampai 4 kali looping
		}
		System.out.println("Jumlah 4 suku pertama: " +(esu-ak)); //Variabel "esu" untuk jumlah semua bilangan dikurang variabel "ak"(bilangan input terbesar)
		System.out.println("Jumlah 4 suku terakhir: " +(esu-it)); //Variabel "esu" untuk jumlah semua bilangan dikurang variabel "it" (bilangan input terkecil)
		//contoh: jika bilangan input = {1,2,3,4,5}
		//dimisalkan khusus untuk komentar: sum(jumlah semua bilangan), max(angka terbesar), min(angka terkecil)
		//1. Jumlah 4 suku pertama: sum - max = (1+2+3+4+5) - 5 = 15 - 5 = 10
		//Bukti cara lain: 4 suku pertama = U1+U2+U3+U4 = 1+2+3+4 = 10
		//2. Jumlah 4 suku terakhir: sum - min = (1+2+3+4+5) -1 = 15 - 1 = 14
		//Bukti cara lain: 4 suku terakhir = U2+U3+U4+U5 = 2+3+4+5 = 14
		}			
	}
}
