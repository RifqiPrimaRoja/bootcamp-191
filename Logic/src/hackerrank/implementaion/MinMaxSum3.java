package hackerrank.implementaion;

import java.util.Scanner;

public class MinMaxSum3 {

	static Scanner ner;
	public static void main(String[] args) {
		//Mendeklarasikan variabel scanner "ner"
		ner = new Scanner(System.in);
		//Memasukkan nilai variabel "bil" dari Scanner yang dimasukkan dalam bentuk array
		//Array yang diinput diberi batasan 5 bilangan
		long bil[] = new long [5];
		
		System.out.println("Masukkan 5 angka: "); {
		long maksim = 0, minim = 0, jumlah = 0;
		bil[0] = maksim = minim = jumlah = ner.nextLong(); //Memasukkan nilai variabel "maksim" = "minim" = jumlah = array bil[0]"dari Scanner dengan tipe data long
		
		//pengulangan dari i=1 sampai dengan i<5, menjadi {1,2,3,4}
		for(int i=1; i<5; i++) {
			bil[i] = ner.nextLong();
			if(bil[i]>maksim) maksim = bil[i];
			if(bil[i]<minim) minim = bil[i];
			jumlah += bil[i]; //akan menjumlahkan terus sampai 4 kali looping
		}
		System.out.println("Jumlah 4 suku pertama: " +(jumlah-maksim)); //Variabel "jumlah" untuk jumlah semua bilangan dikurang variabel "maksim"(bilangan input terbesar)
		System.out.println("Jumlah 4 suku terakhir: " +(jumlah-minim)); //Variabel "jumlah" untuk jumlah semua bilangan dikurang variabel "minim" (bilangan input terkecil)
		//contoh: jika bilangan input = {1,2,3,4,5}
		//1. Jumlah 4 suku pertama: sum - max = (1+2+3+4+5) - 5 = 15 - 5 = 10
		//Bukti cara lain: 4 suku pertama = U1+U2+U3+U4 = 1+2+3+4 = 10
		//2. Jumlah 4 suku terakhir: sum - min = (1+2+3+4+5) -1 = 15 - 1 = 14
		//Bukti cara lain: 4 suku terakhir = U2+U3+U4+U5 = 2+3+4+5 = 14
		}			
	}
}
