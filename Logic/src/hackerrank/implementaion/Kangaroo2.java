package hackerrank.implementaion;

public class Kangaroo2 {
	static String kangaroo(int xa1, int ve1, int xa2, int ve2) {
        String result="YES";
        if(xa1<xa2 && ve1<ve2){ //Apabila posisi dan laju kangguru 1 lebih unggul daripada kangguru 2 maka akan menghasilkan keluaran "NO"
            result="NO";
        } else{
            if(ve1!=ve2 && (xa2-xa1)%(ve1-ve2)==0){ //Apabila laju tidak sama dan percepatan=>0 maka akan menghasilkan keluaran "YES"
                result="YES";
            } else{
                result="NO";
            }
        }
        return result;
    }

    public static void main(String[] args){
    	//Posisi akhir akan menunjukkan posisi danwaktu yang bersamaan yang dilakukan oleh kedua kangguru
        int xa1=0; int ve1=3; //posisi & laju kangguru 1
        int xa2=4; int ve2=2; //posisi & laju kangguru 2
        String answer=kangaroo(xa1, ve1, xa2, ve2);
        System.out.println(answer);
    }
}
