package hackerrank.implementaion;

//Cara 1, menampilkan hasil jumlah, tanpa inputan
//Untuk cara yang dengan inputan ada di pengulangan (kelas) berikutnya
public class SimpleArraySum1 {
	static int simpleArraySum1(int[] array) {
		int jumlah=0; //Inisialisasi awal
		for (int i = 0; i < array.length; i++) {
			jumlah+=array[i]; //Nilai jumlah akan terus dijumlahkan dengan array[i]
		}
		return jumlah; //mengembalikan nilai jumlah ke dlam Array[i]
	}
	public static void main(String[] args) {
		int[] array=new int[] {1,2,3,4,10,11}; //Nilai yang ingin dijumlahkan dalam Array sudah dimasukkan(ditetapkan) didalam program
		int jawab=simpleArraySum1(array);
		System.out.print("Hasil Jumlah semua bilangan : "+jawab);
	}
	
}
