package hackerrank.implementaion;

public class TheHurdleRace3  {

	public static int hurdleRace (int k, int[] t) {
		int maksimal = t[0];
		
		for (int i = 1; i < t.length; i++) {
			if(t[i] > maksimal) {
				maksimal = t[i];
			}
		}
		if(k < maksimal) {
			return maksimal - k;
		}
		else {
			return 0;
		}
	}
	
	
	public static void main(String[] args) {
		System.out.println(hurdleRace(4, new int[] {1,6,3,5,2}));
	}

}