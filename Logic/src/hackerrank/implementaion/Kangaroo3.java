package hackerrank.implementaion;

public class Kangaroo3{
	static String kangaroo(int z1, int v1, int z2, int v2) {
        String result="YES";
        if(z1<z2 && v1<v2){ //Apabila posisi dan laju kangguru 1 lebih unggul daripada kangguru 2 maka akan menghasilkan keluaran "NO"
            result="NO";
        } else{
            if(v1!=v2 && (z2-z1)%(v1-v2)==0){ //Apabila laju tidak sama dan percepatan=>0 maka akan menghasilkan keluaran "YES"
                result="YES";
            } else{
                result="NO";
            }
        }
        return result;
    }

    public static void main(String[] args){
    	//Posisi akhir akan menunjukkan posisi danwaktu yang bersamaan yang dilakukan oleh kedua kangguru
        int z1=0; int v1=3; //posisi & laju kangguru 1
        int z2=4; int v2=2; //posisi & laju kangguru 2
        String result=kangaroo(z1, v1, z2, v2);
        System.out.println(result);
    }
}
