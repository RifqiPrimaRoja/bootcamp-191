package hackerrank.implementaion;

import java.util.Scanner; //Coding untuk memasukkan paket Scanner, agar mempersingkat pengetikan

public class AveryBigSum8 {

	static Scanner result; //Untuk identifikasi scanner, sehingga dapat melakukan deklarasi variabel Scanner berupa variabel "result" dan proses pembuatan objek/input data
	public static void main(String[] args) {
		System.out.println("Input 5 bilangan: ");
		result = new Scanner(System.in); //Mendeklarasikan variabel scanner "result"
		
		//Memasukkan nilai variabel "hr" dari Scanner yang dimasukkan dalam bentuk array
		//Array yang diinput diberi batasan 5 bilangan
		long[] hr = new long[5];
		long h; //Deklarasi variabel "h" dengan tipe data "long" (untuk data yang memiliki range lebih besar dibandingkan "int"
		hr[0] = h = result.nextLong(); //Memasukkan nilai variabel "h" = array hr[0]"dari Scanner dengan tipe data long
		
		//Proses looping, agar setiap pengulangan dijumlahkan sampai batas yang telah ditentukan, yaitu "5 bilangan"
		//"hr.length" nilainya dianggap 5, karena sudah di inisialisasi sebelumnya yang bertuliskan "new long[5]"
		//"hr.length" bisa diinput apabila saat inisialisasi awal bertuliskan "new long[n]" dan ditambah listing program untuk input n
		//pengulangan i = 0, artinya looping dimulai dari angka 0 sampai 5 menjadi:{0,1,2,3,4,5}, totalnya jadi 6 inputan.
		//Agar totalnya menjadi 5 inputan, yaitu dengan cara mengurangi dengan angka 1 pada maksimal looping/"hr.length", menjadi:{0,1,2,3,4}
		for (int i = 0; i < hr.length-1; i++) {
			hr[i] = result.nextLong();
			h = h + hr[i]; //setiap looping, angka akan terus dijumlahkan
		}
		System.out.println("Hasil jumlah 5 bilangan: ");
		System.out.println(h);  //menampilkan hasil penjumlahan 5 bilangan dalam bentuk Array 1 dimensi hr[] yang sudah diubah dalam variabel "h"
	}

}
