package hackerrank.implementaion;

import java.util.Scanner;

public class AveryBigSum2 {

	static Scanner scan; //Untuk identifikasi scanner, sehingga dapat melakukan deklarasi variabel Scanner beruupa variabel "scan" dan proses pembuatan objek/input data
	
	//Fungsi main()
	public static void main(String[] args) {
		System.out.println("Input 5 bilangan: "); //Keterangan, untuk input 5 bilangan yang ingin dijumlahkan
		//Mendeklarasikan variabel scanner "scan"
		scan = new Scanner(System.in);
		//Memasukkan nilai variabel "br" dari Scanner yang dimasukkan dalam bentuk array
		//Array yang diinput diberi batasan 5 bilangan
		long[] br = new long[5];
		
		//inisialisasi awal dengan nilai b mulai dari 0
		//Deklarasi variabel "b" dengan tipe data "long" (untuk data yang memiliki range lebih besar dibandingkan "int"
		//long memiliki ukuran 64 bit, lebih besar dibandingkan int yang hanya 32 bit
		long b = 0;
		//Memasukkan nilai variabel "b" = array br[0]"dari Scanner dengan tipe data long
		br[0] = b = scan.nextLong();
		
		//Proses looping, agar setiap pengulangan dijumlahkan sampai batas yang telah ditentukan, yaitu "5 bilangan"
		//"br.length" nilainya dianggap 5, karena sudah di inisialisasi sebelumnya yang bertuliskan "new long[5]"
		//"br.length" bisa diinput apabila saat inisialisasi awal bertuliskan "new long[n]" dan ditambah listing program untuk input n
		//pengulangan i = 1, artinya looping dimulai dari angka 1 sampai 5, karena jika dimulai dari 0, loopingnya menjadi dari 0, 1, 2, 3, 4, 5, maka totalnya menjadi 6 bilangan yang diinput.
		for (int i = 1; i < br.length; i++) {
			br[i] = scan.nextLong();
					b+= br[i];
		}
		
	System.out.println("Hasil Penjumlahan: ");	//Keterangan, untuk menampilkan hasil penjumlahan
	System.out.println(b); //menampilkan hasil penjumlahan Array 1 dimensi br[] yang sudah diubah dalam variabel "b"
	}

}
