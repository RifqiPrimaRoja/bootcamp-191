package hackerrank.java;

import java.util.Scanner; //Coding untuk memasukkan paket Scanner, agar mempersingkat pengetikan

public class StaticInitializerBlock7 {

	//Deklarasi
	static int a;
	static int b;
	static boolean bangun = true;
	static Scanner nampak;
	public static void main(String[] args) {
		//Sysout: untuk menampilkan teks yang kita ketik secara langsung
		System.out.println("|$$$$$$$$$$$$$$$$$$$$Menentukan Luas Jajar Genjang$$$$$$$$$$$$$$$$$$$$|");
		System.out.println("\n");
		System.out.println("Rumus: ");
		System.out.println("Luas = a x t");
		System.out.println("[Syarat:Breadth and height must be positive(+)!!!]");
		System.out.println("\n");
		
		nampak = new Scanner(System.in);
		System.out.println("Masukkan nilai alas: ");
		a = nampak.nextInt(); //Memasukkan nilai variabel "a" (sebagai alas) dari Scanner
		System.out.println("Masukkan nilai tinggi: ");
		b = nampak.nextInt(); //Memasukkan nilai variabel "b" (sebagai tinggi) dari Scanner
		
		//Pengkondisian untuk menentukan apakah nilai inputan sesuai syarat dan ketentuan atau tidak
		//Jika sesuai syarat maka akan dianggap "True" dan melanjutkan eksekusi
		if(a>0 && b>0) {
			bangun = true;
		}
		else {
			bangun = false;
			System.out.println("java.lang.Exception: Breadth and height must be positive");
		}
		//Karena data inputan sebelumnya sudah sesuai dengan syarat, maka dapat mengeksekusi program dengan rumus dibawah ini.
		if(bangun) {
			int results = a*b;
			System.out.println("Luas Jajar Genjang = ");
			//Untuk menampilkan variabel "results" yaitu berupa luas dari jajar genjang
			System.out.println(results);
		}
			
	}

}
