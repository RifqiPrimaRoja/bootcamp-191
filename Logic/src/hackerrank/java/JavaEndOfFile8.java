package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile8 {
	
static Scanner light;	
public static void main(String[] args) {
	System.out.println("~~~~~Masukkan angka atau kalimat yang ingin ditampilkan~~~~~");
    light = new Scanner(System.in);
    int n=1;//inisialisasi awal looping dimulai dari angka 1
    //"hasNext" untuk mengecek kondisi apakah objek iterator masih punya nilai selanjutnya atau tidak
    //menampilkan outpunya, denganlooping "while"
    while(light.hasNext())
    {
        System.out.println(n+" "+ light.nextLine());
        n++; //Output akan menghasilkan keluaran nomor looping dan kata yang diinput dimulai dari n=1 secara berurutan, seterusnya
    }      
}
}
