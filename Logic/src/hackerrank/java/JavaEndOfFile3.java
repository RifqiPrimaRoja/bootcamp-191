package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile3  {
	
static Scanner cetak;	
public static void main(String[] args) {
	System.out.println("~~~~~Masukkan angka atau kalimat yang ingin ditampilkan~~~~~");
    cetak = new Scanner(System.in);
    int i=1;//inisialisasi awal looping dimulai dari angka 1
    //"hasNext" untuk mengecek kondisi apakah objek iterator masih punya nilai selanjutnya atau tidak
    //menampilkan outputnya, dengan looping "while"
    while(cetak.hasNext())
    {
        System.out.println(i+" "+ cetak.nextLine());
        i++; //Output akan menghasilkan keluaran nomor looping dan kata yang diinput dimulai dari i=1 secara berurutan, seterusnya
    }      
}
}
