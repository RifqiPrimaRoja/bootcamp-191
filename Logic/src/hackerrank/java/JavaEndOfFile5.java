package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile5  {
	
static Scanner gif;	
public static void main(String[] args) {
	System.out.println("~~~~~Masukkan angka atau kalimat yang ingin ditampilkan~~~~~");
    gif = new Scanner(System.in);
    int k=1;//inisialisasi awal looping dimulai dari angka 1
    //"hasNext" untuk mengecek kondisi apakah objek iterator masih punya nilai selanjutnya atau tidak
    //menampilkan outpunya, denganlooping "while"
    while(gif.hasNext())
    {
        System.out.println(k+" "+ gif.nextLine());
        k++; //Output akan menghasilkan keluaran nomor looping dan kata yang diinput dimulai dari k=1 secara berurutan, seterusnya
    }      
}
}

