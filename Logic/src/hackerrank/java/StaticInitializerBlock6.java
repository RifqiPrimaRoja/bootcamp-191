package hackerrank.java;

import java.util.Scanner;

public class StaticInitializerBlock6 {

	//Deklarasi
	static int p;
	static int q;
	static boolean jajaran = true; //deklarasi nilai True dan False
	static Scanner tampil; //Untuk identifikasi scanner, sehingga dapat melakukan deklarasi variabel Scanner beruupa variabel "tampil" dan proses pembuatan objek/input data
	public static void main(String[] args) {
		//Sysout: untuk menampilkan teks yang kita ketik secara langsung
		System.out.println("{####################>Menentukan Luas Jajar Genjang<####################}");
		System.out.println("\n");
		System.out.println("Rumus: ");
		System.out.println("Luas = a x t");
		System.out.println("[Syarat:Breadth and height must be positive(+)!!!]");
		System.out.println("\n");

		//Mendeklarasikan variabel scanner "tampil"
		tampil = new Scanner(System.in);
		System.out.println("Input alas : ");
		p = tampil.nextInt(); //Memasukkan nilai variabel "p" (sebagai alas) dari Scanner
		System.out.println("Input tinggi : ");
		q = tampil.nextInt(); //Memasukkan nilai variabel "y" (sebagai tinggi) dari Scanner
		
		//Pengkondisian untuk menentukan apakah nilai inputan sesuai syarat dan ketentuan atau tidak
		//Jika sesuai syarat maka akan dianggap "True" dan melanjutkan eksekusi
		if(p>0 && q>0) {
			jajaran = true;
		}
		else {
			jajaran = false;
			System.out.println("[Breadth and height must be positive!!!]");
		}
		//Karena data inputan sebelumnya sudah sesuai dengan syarat, maka dapat mengeksekusi program dengan rumus dibawah ini.
		if(jajaran) {
			int result = p*q;
			System.out.print("Luas Jajar Genjang = ");
			//Untuk menampilkan variabel "result" yaitu berupa luas dari jajar genjang
			System.out.println(result);
		}
				
	}

}
