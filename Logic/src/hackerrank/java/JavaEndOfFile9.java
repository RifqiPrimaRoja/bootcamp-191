package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile9  {
	
static Scanner tampil;	
public static void main(String[] args) {
	System.out.println("~~~~~Masukkan angka atau kalimat yang ingin ditampilkan~~~~~");
    tampil = new Scanner(System.in);
    int o=1;//inisialisasi awal looping dimulai dari angka 1
    //"hasNext" untuk mengecek kondisi apakah objek iterator masih punya nilai selanjutnya atau tidak
    //menampilkan outpunya, denganlooping "while"
    while(tampil.hasNext())
    {
        System.out.println(o+" "+ tampil.nextLine());
        o++; //Output akan menghasilkan keluaran nomor looping dan kata yang diinput dimulai dari o=1 secara berurutan, seterusnya
    }      
}
}
