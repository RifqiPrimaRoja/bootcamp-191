package hackerrank.java;

import java.util.Scanner;

//Class(kelas) merupakan rancangan dari objek / menampung informasi
//Objek adalah blueprint(bentukan) dari kelas
public class StaticInitializerBlock5 {

	//Deklarasi
	static int x; //Deklarasi alas dengan tipe data integer(bilangan bulat)----> berupa variabel x
	static int y; //Deklarasi tinggi dengan tipe data integer (bilangan bulat) ----> berupa variabel y
	static boolean jagen = true;
	static Scanner cetak;
	public static void main(String[] args) {
		System.out.println("------------------->Menentukan Luas Jajar Genjang<-------------------");
		System.out.println("\n");
		System.out.println("Rumus: ");
		System.out.println("Luas = a x t");
		System.out.println("[Syarat: a dan t harus berupa bilangan bulat positif(+)]");
		
		//Mendeklarasikan variabel scanner "cetak"
		cetak = new Scanner(System.in);
		System.out.println("Masukkan alas: ");
		x = cetak.nextInt();  //Memasukkan nilai variabel "x" (sebagai alas) dari Scanner
		System.out.println("Masukkan tinggi: ");
		y = cetak.nextInt(); //Memasukkan nilai variabel "y" (sebagai tinggi) dari Scanner
		
		//Pengkondisian untuk menentukan apakah nilai inputan sesuai syarat dan ketentuan atau tidak
		//Jika sesuai syarat maka akan dianggap "True" dan melanjutkan eksekusi
		if(x>0 && y>0) {
			jagen = true;
		}
		
		else {
			jagen = false;
			System.out.println("\n");
			System.out.println("alas dan tinggi inputan anda tidak sesuai syarat!!!");
		}
		
		//Karena data inputan sebelumnya sudah sesuai dengan syarat, maka dapat mengeksekusi program dengan rumus dibawah ini.
		if(jagen){
			int luasan = x*y;
			System.out.print("Luas Jajar Genjang = ");
			//Untuk menampilkan variabel "luasan" yaitu berupa luas dari jajar genjang
			System.out.println(luasan);
		}
		

	}//Penutup method main()

}//Penutup kelas StaticInitialBlock5
