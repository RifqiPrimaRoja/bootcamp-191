package hackerrank.java;

import java.util.Scanner;

public class StaticInitializerBlock {
	//Deklarasi variabel
	static int B;
	static int H;
	static boolean flag=true; //boolean adalah tipe data unik untuk menempatkan nilai True atau False
	static Scanner scn;
	public static void main(String[] args) {
		System.out.println("~~~~~~~~~~~~~~~~~~~Menentukan Luas Jajar Genjang~~~~~~~~~~~~~~~~~~~");
		System.out.println("\n");
		System.out.println("Rumus: ");
		System.out.println("Luas = a x t");
		System.out.println("Keterangan: a dan t harus berupa bilangan positif(+)");
		
		//Scanner: perintah untuk memuat objek/ untuk input data
		scn = new Scanner(System.in);
		System.out.print("Masukkan nilai a(alas) = ");
		//Menggunakan Scanner dan menyimpan apa yang diketik di variabel "B"
		B = scn.nextInt(); //B sebagai lebar(alas jajar genjang)
		System.out.print("Masukkan nilai t(tinggi) = ");
		H = scn.nextInt(); //H sebagai tinggi jajar genjang
		
		//Pengkondisian, jika nilai a dan t lebih besar dari 0, maka "true"/"benar", sehingga akan langsung dieksekusi(dihitung) nilai luas jajar genjang.
		//B sebagai alas dan H sebagai tinggi
		if(B>0 && H>0)
			flag=true;
		//Jika nilai a dan t yang di input kurang dari 0, maka akan tampil keterangan "Lebar dan tinggi harus bilangan positif"
		else
		{
			flag=false;
			System.out.println("java.lang.Exception: Breadth and height must be positive");
			System.out.println("Lebar dan tinggi Jajar genjang harus bilangan positif");
		}
		//Jika dalam kondisi sebelumnya sudah "true", maka langsung menghitung nilai luas seperti dibawah ini:
		if (flag) {
			int area = B*H; //rumus menghitung luas jajar genjang 
			System.out.print("Luas Jajar Genjang = ");
			System.out.println(area); //Variabel area sebagai hasil luas jajar genjang yang akan ditampilkan hasilnya.
		}

	}//Penutup method main()

}//penutup kelas StaticInitializerBlock
