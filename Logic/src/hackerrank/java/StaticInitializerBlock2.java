package hackerrank.java;

import java.util.Scanner;

public class StaticInitializerBlock2 {
	//Deklarasi Variabel
	static int alas;
	static int tinggi;
	static boolean genjang = true;  //boolean adalah tipe data unik untuk menempatkan nilai True atau False
	static Scanner scan;
	
	public static void main(String[] args) {
		System.out.println("~~~~~~~~~~~~~~~~~~~Menentukan Luas Jajar Genjang~~~~~~~~~~~~~~~~~~~");
		System.out.println("\n");
		System.out.println("Rumus: ");
		System.out.println("Luas = alas x tinggi");
		System.out.println("Syarat: Nilai alas dan tinggi yang anda masukkan harus bilangan positif(+)");
		
		//Scanner: perintah untuk memuat objek/ untuk input data
		scan = new Scanner(System.in);
		System.out.print("Masukkan nilai alas = ");
		alas = scan.nextInt(); //input data alas berupa integer (bilangan bulat)
		System.out.print("Masukkan nilai tinggi = ");
		tinggi = scan.nextInt(); //input nilai tinggi dengan tipe data integer (bilangan bulat)
		
		//"genjang" untuk pengkondisian jika benar a dan t yang diinput lebih besar dari 0 maka kan melanjutkan eksekusi
		if(alas>0 && tinggi>0) {
			genjang = true;
		}
		//jika a dan t kurang dari 0 maka "genjang" dianggap "false"
		else {
			genjang = false;
			System.out.println("\n");
			System.out.println("java.lang.Exception: Breadth and height must be positive!!!");
			System.out.println("Lebar dan tinggi Jajar genjang yang anda masukkan tidak sesuai dengan syarat!!!");
		}
		//Eksekusi "genjang" dilakukan seperti dibawah ini, jika a dan t lebih besar dari 0 (true)
		if(genjang) {
			int area = alas*tinggi;
			System.out.print("Luas Jajar Genjang = ");
			System.out.println(area);
		}
	}

}
