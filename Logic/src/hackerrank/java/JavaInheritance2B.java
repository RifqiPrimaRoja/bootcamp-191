package hackerrank.java;

class Arithmetics {
	int add (int e, int f) {
		return e + f;
	}
}

class Adders extends Arithmetics {}

public class JavaInheritance2B {

	public static void main(String[] args) {
		//Membuat objek baru
		Adders e = new Adders();
		
		//Mencetak nama super class pada baris baru
		System.out.println("My superclass is: " + e.getClass().getSuperclass().getName());

		//Mencetak hasil dari 3 panggilan ke metode add (add, int) "Adders" sebagai 3 bilangan bulat yang dipisahkan ruang;
		System.out.println(e.add(10,32) + " " + e.add(10,3) + " " + e.add(10,10) + "\n");
	}

}
