package hackerrank.java;

import java.util.Scanner;

//Class(kelas) merupakan rancangan dari objek / menampung informasi
//Objek adalah blueprint(bentukan) dari kelas
public class StaticInitializerBlock4 {
	//Deklarasi
	static int as; //Deklarasi alas dengan tipe data integer(bilangan bulat)
	static int gi; //Deklarasi tinggi dengan tipe data integer (bilangan bulat)
	static boolean jajargenjang = true; //Boolean: Tipe data bernilai "True" atau "False"
	static Scanner an;
	public static void main(String[] args) {
		System.out.println("~~~~~~~~~~~~~~~~~~~Menentukan Luas Jajar Genjang~~~~~~~~~~~~~~~~~~~");
		System.out.println("\n");
		System.out.println("Rumus: ");
		System.out.println("Luas = a x t");
		System.out.println("Ketentuan: a dan t harus berupa bilangan positif(+)");
		
		//Mendeklarasikan variabel scanner "an"
		an = new Scanner(System.in);
		System.out.println("Masukkan alas jajar genjang : ");
		as = an.nextInt(); //Memasukkan nilai variabel "as" (sebagai alas) dari Scanner
		System.out.println("Masukkan tinggi jajar genjang : ");
		gi = an.nextInt(); //Memasukkan nilai variabel "gi" (sebagai tinggi) dari Scanner
		
		//Pengkondisian untuk menentukan apakah nilai inputan sesuai syarat dan ketentuan atau tidak
		//Jika sesuai syarat maka akan dianggap "True" dan melanjutkan eksekusi
		if(as>0 && gi>0) {
			jajargenjang = true;
		}
		else {
			jajargenjang = false;
			System.out.println("\n");
			System.out.println("~~~~~alas dan tinggi yang anda input tidak sesuai dengan ketentuan~~~~~");
		}
		//Karena data inputan sebelumnya sudah sesuai dengan syarat, maka dapat mengeksekusi program dengan rumus dibawah ini.
		if(jajargenjang){
			int hasil = as*gi;
			System.out.println("Luas Jajar Genjang : ");
			System.out.println(hasil); //Untuk menampilkan variabel "hasil" yaitu berupa luas dari jajar genjang
		}
		

	}//Penutup method main()

}//Penutup kelas StaticInitialBlock4
