package hackerrank.java;

class Math {
	int plus(int k, int l) {
		return k + l;
	}
}

class Ad extends Math{}

public class JavaInheritance2D {

	public static void main(String[] args) {
		//membuat objek baru
		Ad k = new Ad();
		
		//Mencetak nama superclass pada baris baru
		//karena Class "Math" berada didalam package "hackerrank.java", maka akan tampil: "My Superclass is: hackerrank.java.Math"
		System.out.println("My superclass is: " + k.getClass().getSuperclass().getName());
	
		//Mencetak hasil dari 3 panggilan ke metode plus (plus,int) "Math" sebagai 3 bilangan bulat yang dipisahkan ruang
		//Mencetak langsung hasil penjumlahan, misal "k.plus(10,32)", artinya 10 + 32 = 42, untuk "k.plus(10,3), artinya 10 + 3 = 13, untuk "k.plus(10,10), artinya 10 + 10 = 20
		//Maka outpunya adalah: 42 13 20
		System.out.println(k.plus(10,32) + " " + k.plus(10,3) + " " + k.plus(10,10)  + "\n");
	}

}
