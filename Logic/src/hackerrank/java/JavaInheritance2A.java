package hackerrank.java;

class Arithmetic {
	int add (int c, int d) {
		return c + d;
	}
}

class Adder extends Arithmetic {}

public class JavaInheritance2A {

	public static void main(String[] args) {
		//Membuat objek baru
		Adder c = new Adder();
		
		//Mencetak nama super class pada baris baru
		System.out.println("My superclass is: " + c.getClass().getSuperclass().getName());

		//Mencetak hasil dari 3 panggilan ke metode add (add, int) Adder sebagai 3 bilangan bulat yang dipisahkan ruang;
		System.out.println(c.add(10,32) + " " + c.add(10,3) + " " + c.add(10,10) + "\n");
	}

}
