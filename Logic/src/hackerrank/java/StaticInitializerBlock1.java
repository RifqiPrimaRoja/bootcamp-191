package hackerrank.java;

import java.util.Scanner;

public class StaticInitializerBlock1 {
	
		//Deklarasi Variabel
		static int a;
		static int t;
		static boolean jajar = true;
		static Scanner scn;
		
		public static void main(String[] args) {
			System.out.println("~~~~~~~~~~~~~~~~~~~Menentukan Luas Jajar Genjang~~~~~~~~~~~~~~~~~~~");
			System.out.println("\n");
			System.out.println("Rumus: ");
			System.out.println("Luas = a x t");
			System.out.println("Keterangan: a dan t harus berupa bilangan positif");
			
			//Scanner: perintah untuk memuat objek/ untuk input data
			scn = new Scanner(System.in);
			System.out.println("Masukkan nilai a: ");
			a = scn.nextInt(); //Menggunakan scanner dan mengambil data dengan tipe integer
			System.out.println("Masukkan nilai t: ");
			t = scn.nextInt();
			
			if(a>0 && t>0)
				jajar = true;
			else {
				jajar = false;
				System.out.println("java.lang.Exception: Breadth and height must be positive");
				System.out.println("Maaf, nilai a dan t yang anda masukkan harus positif(+)");
			}
			if(jajar) {
				int area = a*t; //rumus menghitung luas jajar genjang----> Luas = alas x tinggi
				System.out.print("Luas jajar genjang = ");
				System.out.println(area);
			}
			
		}

	}


