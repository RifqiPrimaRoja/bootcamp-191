package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile6  {
	
static Scanner gmb;	
public static void main(String[] args) {
	System.out.println("<-+-+-+-+-+-+-+-Masukkan angka atau kalimat yang ingin ditampilkan-+-+-+-+-+-+-+->");
    gmb = new Scanner(System.in);
    int l=1;//inisialisasi awal looping dimulai dari angka 1
    //"hasNext" untuk mengecek kondisi apakah objek iterator masih punya nilai selanjutnya atau tidak
    //menampilkan outpunya, denganlooping "while"
    while(gmb.hasNext())
    {
        System.out.println(l+" "+ gmb.nextLine());
        l++; //Output akan menghasilkan keluaran nomor looping dan kata yang diinput dimulai dari l=1 secara berurutan, seterusnya
    }      
}
}

