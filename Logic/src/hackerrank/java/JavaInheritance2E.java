package hackerrank.java;

class Angka {
	int max (int x, int y) {
		return x + y;
	}
}

class Jadi extends Angka {}

public class JavaInheritance2E {

	public static void main(String[] args) {
		//membuat objek baru
		Jadi x = new Jadi();
		
		//mencetak nama superclass pada baris baru
		//Karena class "Angka" berada didalam package "hackerrank.java", maka akan tampil "My superclass is: hackerrank.java.Angka".
		System.out.println("My superclass is: " + x.getClass().getSuperclass().getName());

		System.out.println(x.max(10,32) + " " + x.max(10,3) + " " + x.max(10, 10) + "\n");
	}

}
