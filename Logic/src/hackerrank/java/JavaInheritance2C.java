package hackerrank.java;

class Aritmat {
	int add(int g, int h) {
		return g + h;
	}
}

class Tambah extends Aritmat {}

public class JavaInheritance2C {

	public static void main(String[] args) {
		//Membuat objek baru
		Tambah g = new Tambah();
		
		//Mencetak nama superclass pada baris baru
		//karena Class "Aritmat" berada didalam package "hackerrank.java", maka akan tampil: "My Superclass is: hackerrank.java.Aritmat"
		System.out.println("My superclass is: " + g.getClass().getSuperclass().getName());
		
		//Mencetak hasil dari 3 panggilan ke metode add (add,int) "Tambah" sebagai 3 bilangan bulat yang dipisahkan ruang
		//Mencetak langsung hasil penjumlahan, misal "g.add(10,32)", artinya 10 + 32 = 42, untuk "g.add(10,3), artinya 10 + 3 = 13, untuk "g.add(10,10), artinya 10 + 10 = 20
		//Maka outputnya adalah: 42 13 20
		System.out.println(g.add(10,32) + " " + g.add(10,3) + " " + g.add(10,10) + "\n");
		

	}

}
