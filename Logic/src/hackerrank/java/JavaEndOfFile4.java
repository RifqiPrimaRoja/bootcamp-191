package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile4  {
	
static Scanner jpg;	
public static void main(String[] args) {
	System.out.println("~~~~~Masukkan angka atau kalimat yang ingin ditampilkan~~~~~");
    jpg = new Scanner(System.in);
    int j=1;//inisialisasi awal looping dimulai dari angka 1
    //"hasNext" untuk mengecek kondisi apakah objek iterator masih punya nilai selanjutnya atau tidak
    //menampilkan outpunya, denganlooping "while"
    while(jpg.hasNext())
    {
        System.out.println(j+" "+ jpg.nextLine());
        j++; //Output akan menghasilkan keluaran nomor looping dan kata yang diinput dimulai dari j=1 secara berurutan, seterusnya
    }      
}
}
