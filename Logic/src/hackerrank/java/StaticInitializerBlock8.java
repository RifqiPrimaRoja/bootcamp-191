package hackerrank.java;

import java.util.Scanner; //Coding untuk memasukkan paket Scanner, agar mempersingkat pengetikan

public class StaticInitializerBlock8 {

	//Deklarasi
		static int t;
		static int u;
		static boolean ruang = true;
		static Scanner tampak;
		public static void main(String[] args) {
			//Sysout: untuk menampilkan teks yang kita ketik secara langsung
			System.out.println("{####################>Menentukan Luas Jajar Genjang<####################}");
			System.out.println("\n");
			System.out.println("Rumus: ");
			System.out.println("Luas = a x t");
			System.out.println("[Syarat:Breadth and height must be positive(+)!!!]");
			System.out.println("\n");
			
			//Mendeklarasikan variabel scanner "tampak"
			tampak = new Scanner(System.in);
			System.out.println("Masukkan nilai alas: ");
			t = tampak.nextInt(); //Memasukkan nilai variabel "t" (sebagai alas) dari Scanner
			System.out.println("Masukkan nilai tinggi: ");
			u = tampak.nextInt(); //Memasukkan nilai variabel "u" (sebagai tinggi) dari Scanner
			
			//Pengkondisian untuk menentukan apakah nilai inputan sesuai syarat dan ketentuan atau tidak
			//Jika sesuai syarat maka akan dianggap "True" dan melanjutkan eksekusi
			if(t>0 && u>0) {
				ruang = true;
			}
			else {
				ruang = false;
				System.out.println("java.lang.Exception: Breadth and height must be positive");
			}
			//Karena data inputan sebelumnya sudah sesuai dengan syarat, maka dapat mengeksekusi program dengan rumus dibawah ini.
			if(ruang) {
				int results = t*u;
				System.out.println("Luas Jajar Genjang = ");
				//Untuk menampilkan variabel "results" yaitu berupa luas dari jajar genjang
				System.out.println(results);
			}
				
		} //Penutup method main()

} //penutup kelas StaticInitializerBlock8
