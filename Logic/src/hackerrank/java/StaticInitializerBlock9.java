package hackerrank.java;

import java.util.Scanner;

public class StaticInitializerBlock9  {

	//Deklarasi
		static int y; //deklarasi alas y
		static int z; //deklarasi tinggi z
		static boolean dimensitiga = true;
		static Scanner nn;
		public static void main(String[] args) {
			//Sysout: untuk menampilkan teks yang kita ketik secara langsung
			System.out.println("{+-+-+-+-+-+-+-+-+-+-Menentukan Luas Jajar Genjang-+-+-+-+-+-+-+-+-+-+}");
			System.out.println("\n");
			System.out.println("Rumus: ");
			System.out.println("Luas = a x t");
			System.out.println("[Syarat:Breadth and height must be positive(+)!!!]");
			System.out.println("\n");
			
			//Mendeklarasikan variabel scanner "nn"
			nn = new Scanner(System.in);
			System.out.println("Masukkan nilai alas: ");
			y = nn.nextInt(); //Memasukkan nilai variabel "y" (sebagai alas) dari Scanner
			System.out.println("Masukkan nilai tinggi: ");
			z = nn.nextInt(); //Memasukkan nilai variabel "z" (sebagai tinggi) dari Scanner
			
			//Pengkondisian untuk menentukan apakah nilai inputan sesuai syarat dan ketentuan atau tidak
			//Jika sesuai syarat maka akan dianggap "True" dan melanjutkan eksekusi
			//Pada contoh pengulangan terakhir ini, pengkondisian hanya di balik saja
			//Jika angka inputan < 0, maka akan muncul keterangan seperti dibawah ini
			if(y<0 && z<0) {
				dimensitiga = false;
				System.out.println("java.lang.Exception: Breadth and height must be positive");
			}
			//Jika benar angka input > 0, maka akan langsung bernilai "true" dan akan dieksekusi
			else {
				dimensitiga = true;
				
			}
			//Karena data inputan sebelumnya sudah sesuai dengan syarat, maka dapat mengeksekusi program dengan rumus dibawah ini.
			if(dimensitiga) {
				int jawab = y*z;
				System.out.println("Luas Jajar Genjang = ");
				//Untuk menampilkan variabel "jawab" yaitu berupa luas dari jajar genjang
				System.out.println(jawab);
			}
				
		} //Penutup method main()

} //penutup kelas StaticInitializerBlock9
