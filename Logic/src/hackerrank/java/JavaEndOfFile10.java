package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile10 {
	
static Scanner copy;	
public static void main(String[] args) {
	System.out.println("~~~~~Masukkan angka atau kalimat yang ingin ditampilkan~~~~~");
    copy = new Scanner(System.in);
    int loop=1;//inisialisasi awal looping dimulai dari angka 1
    //"hasNext" untuk mengecek kondisi apakah objek iterator masih punya nilai selanjutnya atau tidak
    //menampilkan outpunya, denganlooping "while"
    while(copy.hasNext())
    {
        System.out.println(loop+" "+ copy.nextLine());
        loop++; //Output akan menghasilkan keluaran nomor looping dan kata yang diinput dimulai dari loop=1 secara berurutan, seterusnya
    }      
}
}
