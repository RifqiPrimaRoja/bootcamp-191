package hackerrank.java;

import java.util.Scanner;

public class JavaEndOfFile7  {
	
static Scanner jpeg;	
public static void main(String[] args) {
	System.out.println("~~~~~Masukkan angka atau kalimat yang ingin ditampilkan~~~~~");
    jpeg = new Scanner(System.in);
    int m=1;//inisialisasi awal looping dimulai dari angka 1
    //"hasNext" untuk mengecek kondisi apakah objek iterator masih punya nilai selanjutnya atau tidak
    //menampilkan outpunya, denganlooping "while"
    while(jpeg.hasNext())
    {
        System.out.println(m+" "+ jpeg.nextLine());
        m++; //Output akan menghasilkan keluaran nomor looping dan kata yang diinput dimulai dari =1 secara berurutan, seterusnya
    }      
}
}
