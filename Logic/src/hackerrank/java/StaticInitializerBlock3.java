package hackerrank.java;

import java.util.Scanner;

public class StaticInitializerBlock3 {
		//Deklarasi
		static int al;
		static int ti;
		static boolean jajang = true; //boolean adalah tipe data unik yang hanya punya 2 nilai yaitu True atau False
		static Scanner ner;
		public static void main(String[] args) {
			System.out.println("~~~~~~~~~~~~~~~~~~~Menentukan Luas Jajar Genjang~~~~~~~~~~~~~~~~~~~");
			System.out.println("\n");
			System.out.println("Rumus: ");
			System.out.println("Luas = a x t");
			System.out.println("Keterangan: a dan t harus berupa bilangan positif(+)");

		//Scanner: perintah untuk memuat objek/ untuk input data
		ner = new Scanner(System.in);
		System.out.println("Masukkan alas: ");
		//Menggunakan Scanner dan menyimpan apa yang diketik di variabel "al"
		al = ner.nextInt(); //al sebagai lebar(alas jajar genjang)
		System.out.println("Masukkan tinggi: ");
		ti = ner.nextInt(); //ti sebagai tinggi jajar genjang)
		
		//Pengkondisian "True" atau "false" yang sebelumnya harus menggunakan tipe data Boolean
		if(al>0 && ti>0) {
			jajang = true;
			
		}
		else {
			jajang = false;
			System.out.println("\n");
			System.out.println("java.lang.Exception: Breadth and height must be positive!!!");
			System.out.println("Lebar dan tinggi Jajar genjang harus bilangan positif!!!");
		}
		if(jajang) {
			int luas = al*ti;
			System.out.println("Luas Jajar Genjang = ");
			System.out.println(luas);
		}
			

	}

}
