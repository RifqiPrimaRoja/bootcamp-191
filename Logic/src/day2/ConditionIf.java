package day2;

public class ConditionIf
{
	public static void main(String[] args)
	{
		//initial variable x with data type int
		int x = 10;
		//condition if value of variable x less then 20
		if (x < 20)
		{
			System.out.println("This is if x value less then 20");
		}
	}
}
