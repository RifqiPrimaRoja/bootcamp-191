package day2;

import java.util.Scanner;

public class Soal6 {
	static Scanner scn;
	
	public static void main(String[] args) {
		scn = new Scanner(System.in);
		System.out.println("Masukkan Input : ");
		String s=scn.next();
		int u=0;
		
		for (int i = 0; i < s.length(); i++) {
			if(Character.isUpperCase(s.charAt(i))) {
				u++;
			}
		}
		System.out.println("Output: "+u);
	}	
}