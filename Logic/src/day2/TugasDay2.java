package day2;

import java.util.Scanner;

public class TugasDay2
{
	//method main
	static Scanner input;
	public static void main(String[] args)
	{	
		input = new Scanner(System.in);
		int a = 0;
		System.out.println("~~~~~~~~~~~~~~~~~~~~Tugas Day 2 - Soal Deret~~~~~~~~~~~~~~~~~~~~");
	//Nomor 1
		//1 3 9 27 81 243 729
		int b = 1;
		System.out.println(" ");
		System.out.println("[Nomor 1]");
		System.out.print("Jumlah Looping: ");
		int p1 = input.nextInt();
		/*System.out.print("Jumlah Looping ke-2: ");
		//int p2 = input.nextInt();
		for (int i=0; i<p1; i++)
		{
			System.out.print(b + " ");
			b=b*3;
		}*/
		int x0=b; int x1=b; int x2=b;
		System.out.print("Masukkan rasio:");
		int p2 = input.nextInt();
		for (int i=0; i<p1; i++)
		{
			if (i==0)
			{
				x0=b;
			}
			else if (i==1)
			{
				x1=b;
			}
			else if (i==2)
			{
				x2=b;
			}
			System.out.print(b + " ");
			b = b*p2;
		
		}
			System.out.println(" ");
			int hitung = x0 + x1 + x2;
			System.out.println("Hasil perhitungan:  " +x0+ "+" +x1+ "+" +x2+ "=" +hitung);
			
		//Nomor 2
			//1 3 9 27 81 243 729
			int c = 1;
			System.out.println(" ");
			System.out.println("[Nomor 2]");
			System.out.print("Jumlah Looping: ");
			int q1 = input.nextInt();
			
			int y0=c; int y1=c; int y2=c;
			System.out.print("Masukkan rasio:");
			int q2 = input.nextInt();
			for (int i=0; i<q1; i++)
			{
				if (i==0)
				{
					y0=c;
				}
				else if (i==1)
				{
					y1=c;
				}
				else if (i==2)
				{
					y2=-c;
				}
					if ((i+1)%3 == 0)
					{
						System.out.print(-c + " ");
					}
					else
					{
						System.out.print(c + " ");
						//c = c*q2;
					}
					c = c*q2;
			
			}
				System.out.println(" ");
				int hitung1 = y0 * y1 * y2;
				System.out.println("Hasil perhitungan:  " +y0+ "x" +y1+ "x" +y2+ "=" +hitung1);
				
			
			//Nomor 3
			//2 6 12 24 12 6 2
				input = new Scanner(System.in);
				System.out.println(" ");
				System.out.println("[Nomor 3]");
				System.out.println("Jumlah Looping: ");
				int r1 = input.nextInt();
				System.out.println("Masukkan N2: ");
				int r2 = input.nextInt();
				System.out.println("Masukkan N3: ");
				int r3 = input.nextInt();
				int d = 2; int z1 = d; int z2 = d; int z3 = d;
				for (int i=0; i<r1; i++)
				{
					if (i==1)
					{
						z1=d;
					}
					else if (i==2)
					{
						z2=d;
					}
					else if (i==3)
					{
						z3=d;
					}
						if (i==0)
						{
							System.out.print(d+ " ");
							d=d*r2;
						}
						else if (i<3)
						{
							System.out.print(d+ " ");
							d=d*r3;
						}
						else if (i<5)
						{
							System.out.print(d+ " ");
							d=d/r3;
						}
						else if (i<r1)
						{
							System.out.print(d+ " ");
							d=d/r2;
						}
				}
				
				System.out.println("");
				
				int hitung4=z3-z2-z1;
				System.out.println(z3+" - "+z2+" - "+z1+" = "+hitung4);
				
			//Nomor 4
			//1 5 2 10 3 15 4
	
				System.out.println(" ");
				System.out.println("[Nomor 4]");
				System.out.print("Jumlah Looping: ");
				int s1 = input.nextInt();
				System.out.println("Masukkan N2: ");
				int z = input.nextInt();
				int e = 1; int f=0;
				int hitung3 = 0;
				for (int i=0; i<s1; i++)
				{
					if (i%2==0)
					{
						System.out.print(e + " ");
						e=e+1;
					}
					else
					{
					
						f=f+z;
						System.out.print(f + " ");
						hitung3+=f;
					}
				}
				System.out.println(" ");
				System.out.println("Hasil Perhitungan:" +hitung3);
				
			//Nomor 6
					System.out.println(" ");
					System.out.println("[Nomor 6]");
					input = new Scanner(System.in);
					System.out.println("Masukkan Input : ");
					String s=input.next();
					int v=0;
					
					for (int i = 0; i < s.length(); i++) 
					{
						if(Character.isUpperCase(s.charAt(i)))
						{
							v++;
						}
					}
					System.out.println("Output: " +v);
				
	}

}
