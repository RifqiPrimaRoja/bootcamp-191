package day2;

public class ConditionIfElse 
{
	public static void main(String[] args)
	{
		//initial variable x with data type int
		int x = 10;
		//condition if value of variable x less then 20
		if (x < 20)
		{
			System.out.println("This is if x value less then 20");
		}
		
		/*else if (x = 20)
		{
			System.out.println("This is if x value equal 20");
		}
		
		else
		{
			System.out.println("This is if x value more then 20");
		}*/
		//statement if condition false
		else
		{
			System.out.println("This is x value more and equal 20");
		}

	}
}
