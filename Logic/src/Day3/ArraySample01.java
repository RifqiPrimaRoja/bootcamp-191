package Day3;

import java.util.Scanner;

public class ArraySample01
{
	
	static Scanner scn;
	public static void main(String[] args)
	{
		// 3 1 9 3 15 5 => n=6
		scn = new Scanner(System.in);
		System.out.println("Masukkan nilai n: ");
		int n = scn.nextInt();
		
		int[] array = new int[n];
		int angka1 = 3;
		int angka2 = 1;
		//array[0]=3;
		//array[1]=1;
		//array[2]=9;
		//array[3]=3;
		//array[4]=15;
		//array[5]=5;
//		array[i]=angka1;
		System.out.println(array.length);
		for (int i = 0; i < array.length; i++)
		{ 
			//mengisi array
			if(i%2==0)
			{
				array[i] = angka1;
				angka1 = angka1 + 6;
			}
			else
			{
				array[i] = angka2;
				angka2 = angka2 + 2;
			}
			//tampilkan array ke layar
			System.out.print(array[i]+ "\t");
		}
		

	}

}
