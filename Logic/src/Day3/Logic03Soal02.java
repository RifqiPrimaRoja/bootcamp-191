package Day3;

import java.util.Scanner;

import common.PrintArray;

public class Logic03Soal02 
{
	static Scanner scn;
	public static void main(String[] args) 
	{
		scn = new Scanner(System.in);
		
		System.out.print("Masukkan N : ");
		int n = scn.nextInt();
		System.out.print("Masukkan M : ");
		int m = scn.nextInt();
		System.out.print("Masukkan O : ");
		int o = scn.nextInt();
		
		int index = 0;
		int[] deret = new int[n*3];
		int angka = o;
		
		//2. buat array 2 dimensi
		//Nilai Array
		for(int i=0; i<deret.length;i++)
		{ 
			if(i%4==3) 
			{
				deret[i]=m;
			}
			else
			{
				deret[i]=angka;
				angka=angka+m;
			}
		}
		String[][] array = new String[n][n];
		//mengisi array
		for (int i=0; i<n; i++)
		{
			array[n-1-i][i] = deret[index]+" ";
			index++;
		}
		
		//Array 2
		for (int i=1; i<n; i++) 
		{
			array[i][n-1] = deret[index]+" ";
			index++;
		}
				
		//Array 3
		for(int i=n-2; i>0; i--)
		{
			array[n-1][i] = deret[index]+" ";
			index++;
		}
		
		//menampilkan array
		PrintArray.array2D(array);
		
	}

}
