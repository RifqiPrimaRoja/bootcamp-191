package Day3;

import java.util.Scanner;

public class Logic03Soal01 
{
	static Scanner scn;
	public static void main(String[] args) 
	{
		scn = new Scanner(System.in);
		
		System.out.println("Masukkan N : ");
		int n = scn.nextInt();
		System.out.println("Masukkan M : ");
		int m = scn.nextInt();
		System.out.println("Masukkan O : ");
		int o = scn.nextInt();
		
		//1. buat array deret
		int[] deret = new int[n*4];
		int angka = o;
		for (int i = 0; i < deret.length; i++)
		{
			//deret[i] = angka;
			//angka = angka + n;
			if (i%4==3)
			{
				deret[i] = m;
			}
			else
			{
				deret[i] = angka;
				angka = angka + m;
			}
			//System.out.print(deret[i]+ "\t");
		}
		//2. BUat array 2 dimensi
		String[][] array = new String[n][n];
		//4. membuat index
		int index=0;
		
		//3. Isi baris ke 0
		for (int i = 0; i < n; i++)
		{
			array[0][i]=deret[index]+"";
			index++;
			//System.out.print(deret[i]+ "\t");
		}
			System.out.println("\n");
			
		//5. Isi kolom ke n-1
		for (int i = 1; i < n; i++)
		{
			array[i][n-1]=deret[index]+"";
			index++;
			//System.out.print(deret[i]+ "\t");
		}
		
		//6. Isi baris ke n-1
		for (int i = n-2; i>=0; i--) 
		{
			array[n-1][i]=deret[index]+"";
			index++;
		}
		//7. Isi kolom ke 0
		for (int i = n-2; i>0; i--) 
		{
			array[i][0]=deret[index]+"";
			index++;
		}
			
		//menampilkan array 2 dimensi
		
		for (int b = 0; b < array.length; b++)
		{
			//pring dari kiri ke kanan
			for (int k = 0; k < array[b].length; k++) 
			{
				System.out.print(array[b][k]+ "\t");
			}
			//pindah baris
			System.out.println("\n");
		}

	}

}
