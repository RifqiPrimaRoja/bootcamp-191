package Day3;

import java.util.Scanner;

public class LatihanSoal5 
{
	static Scanner scn;
	public static void main(String[] args)
	{
		scn = new Scanner(System.in);
		System.out.println("Masukkan Text: ");
		String text = scn.nextLine(); //input
		//System.out.println(text.split(" "));
		String[] array = text.split(" "); //tipe data kiri dan kanan sama, yaitu sama2 Array
		for(int i=0; i<array.length; i++)
		{
			String[] item = array[i].split(""); //split word to character
			for(int j=0; j<item.length; j++)
			{
				if(j>0 && j<item.length-1)
				{
					System.out.print("*");
				}
				else
				{
					System.out.print(item[j]); //output character
				}
			}
			System.out.println();
		}
	}

}
