package Day3;

public class Array1D 
{

	public static void main(String[] args)
	{
		//deklarasi array
		//index nya dari 0 - 9
		int[] array = new int[10];
		//mengisi value element array
		array[0]=1;
		array[1]=2;
		array[2]=3;
		array[3]=4;
		array[4]=5;
		array[5]=6;
		array[6]=7;
		array[7]=8;
		array[8]=9;
		array[9]=8;
		System.out.println(array.length);
		for (int i = 0; i < array.length; i++)
		{
			System.out.print(array[i]);
		}
		
		//cara kedua
		System.out.println(" ");
		int[] array2 = new int[] {1,2,3,4,5};
		array[0]=3;
		array[1]=1;
		array[2]=5;
		array[3]=7;
		array[4]=0;
		System.out.println(array2.length);
		for (int i = 0; i < array2.length; i++)
		{
			System.out.print(array2[i]);
		}

	}

}
