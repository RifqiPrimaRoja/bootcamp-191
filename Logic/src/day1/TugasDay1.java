package day1;

public class TugasDay1
{
	//method main
	public static void main(String[] args) 
	{
		//instansiasi
		//1 3 5 7 9 11 13
		int a=1;
		System.out.println("~~~~~~~~~~~~~~~~~~~~Tugas Day 1 - Soal Deret~~~~~~~~~~~~~~~~~~~~");
		System.out.println(" ");
		System.out.println("Deret nomor 1");
		for(int i=0; i<7; i++)
		{
			System.out.print(a +" ");
			a=a+2;
		}
		
        //2 4 6 8 10 12 14
		int b=2;
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("Deret nomor 2");
		for(int i=0; i<7; i++)
		{
			System.out.print(b +" ");
			b=b+2;
		}
		
		//1 4 7 10 13 16 19
		int c=1;
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("Deret nomor 3");
		for(int i=0; i<7; i++)
		{
			System.out.print(c +" ");
			c=c+3;
		}
		
		//1 4 7 10 13 16 19
		int d=1;
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("Deret nomor 4");
		for(int i=0; i<7; i++)
		{
				System.out.print(d +" ");
				d=d+3;
		}
		
		//1 5 * 9 13 * 17
		int e=1;
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("Deret nomor 5");
		for(int i=0; i<7; i++)
		{
			/*if (i==2)
			System.out.print("* 9" +" ");
			else if (i==4)
			System.out.print("* 17" +" ");
			else if (i!=2 || i!=4)
			System.out.print(e +" ");
			e=e+4;*/
			if (i==2 || i==5)
			{
				System.out.print("*" +" ");
			}
			else if (i!=2 || i!=5)
			{
				System.out.print(e + " ");
				e=e+4;
			}
		}
		
		//1 5 9 13 17 21 25
		//1 5 * 13 17 * 25
		int f=1; 
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("Deret nomor 6");
		for(int i=0; i<7; i++)
		{
				if (i==2 || i==5)
				System.out.print("*" +" ");
				else if (i!=2 || i!=5)
				System.out.print(f +" ");
				f=f+4;
		}
		
		//2 4 8 16 32 64 128
		int g=2; 
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("Deret nomor 7");
		for(int i=0; i<7; i++)
		{
				System.out.print(g +" ");
				g=g*2;	
		}
		
		//3 9 27 81 243 729 2187
		int h=3; 
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("Deret nomor 8");
		for(int i=0; i<7; i++)
		{
				System.out.print(h +" ");
				h=h*3;		
		}
		
		//4 16 * 64 256 * 1024
		int j=4;
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("Deret nomor 9");
		for(int i=0; i<7; i++)
		{
			/*if (i==2)
			System.out.print("* 64" +" ");
			else if (i==4)
			System.out.print("* 1024" +" ");
			else if (i!=2 || i!=4)
			System.out.print(j + " ");
			j=j*4;*/
			if (i==2 || i==5)
			{
				System.out.print("*" +" ");
			}
			else if (i!=2 || i!=5)
			{
				System.out.print(j + " ");
				j = j*4;
			}
		}				
		
		//3 9 27 XXX 243 729 2187
		int k=3; 
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("Deret nomor 10");
		for(int i=0; i<7; i++)
		{
				if (i==3)
				System.out.print("XXX" +" ");
				else if (i!=3)
				System.out.print(k +" ");
				k=k*3;		
		}
	}

}
