package day1;

public class Helloworld 
{
	private static int batch;
	public static void main(String[] args) 
	{
		System.out.println("~~~~~~~~~~~~~~~~~~~Welcome to Java~~~~~~~~~~~~~~~~~~~");
		makanfavorit(); 
		sampleobject();
		
	}
	
	public static void makanfavorit()
	{
		System.out.println("");
		System.out.println("Makanan Favoritku:");
		System.out.println("1. Mie Bangka");
		System.out.println("2. Gule Kambing");
		System.out.println("3. Sate Padang");
		System.out.println("4. Ikan Gurame");
		System.out.println("5. Ikan Patin Sayur Bangka");
		System.out.println("");
	}

	public static void sampleobject()
	{
		int anu = 10;
		Orang or01 = new Orang();
		or01.nama="Oja";
		or01.alamat="Bekasi";
		or01.jk="Laki-laki";
		or01.tempatlahir="Bekasi";
		or01.umur=23;
		or01.cetak();
		
		/*System.out.println("Nama:"+or01.nama);
		System.out.println("Alamat:"+or01.alamat);
		System.out.println("JK:"+or01.jk);
		System.out.println("Tempat Lahir:"+or01.tempatlahir);
		System.out.println("Umur:"+or01.umur);
		System.out.println("");*/
		
		Orang or02 = new Orang();
		or02.nama="Putri";
		or02.alamat="Jakarta";
		or02.jk="Wanita";
		or02.tempatlahir="Jakarta";
		or02.umur=22;
		or02.cetak();
		
		/*System.out.println("Nama:"+or02.nama);
		System.out.println("Alamat:"+or02.alamat);
		System.out.println("JK:"+or02.jk);
		System.out.println("Tempat Lahir:"+or02.tempatlahir);
		System.out.println("Umur:"+or02.umur);
		System.out.println("");*/
		
		Orang or03 = new Orang();
		or03.nama="Fikar";
		or03.alamat="Bekasi";
		or03.jk="Wanita";
		or03.tempatlahir="Bekasi";
		or03.umur=24;
		or03.cetak();
		
		/*System.out.println("Nama:"+or03.nama);
		System.out.println("Alamat:"+or03.alamat);
		System.out.println("JK:"+or03.jk);
		System.out.println("Tempat Lahir:"+or03.tempatlahir);
		System.out.println("Umur:"+or03.umur);
		System.out.println("");*/
		
		Orang or04 = new Orang();
		or04.nama="Nopriawan";
		or04.alamat="Jakarta Pusat";
		or04.jk="Laki-laki";
		or04.tempatlahir="Lampung";
		or04.umur=24;
		or04.cetak();
		
		/*System.out.println("Nama:"+or04.nama);
		System.out.println("Alamat:"+or04.alamat);
		System.out.println("JK:"+or04.jk);
		System.out.println("Tempat Lahir:"+or04.tempatlahir);
		System.out.println("Umur:"+or04.umur);
		System.out.println("");*/
		
		Orang or05 = new Orang();
		or05.nama="Hendra";
		or05.alamat="Bogor";
		or05.jk="Laki-laki";
		or05.tempatlahir="Jakarta";
		or05.umur=24;
		or05.cetak();
		
		System.out.println("Nama:"+or05.nama);
		System.out.println("Alamat:"+or05.alamat);
		System.out.println("JK:"+or05.jk);
		System.out.println("Tempat Lahir:"+or05.tempatlahir);
		System.out.println("Umur:"+or05.umur);
		System.out.println();
		
	/*	String nama1 = "Roni";
		String alamat1 = "Jakarta";
		String jk1 = "Pria";
		String tempatlahir1= "Kecubung Raya";
		int umur1 = 20;
		
		String nama2 = "Roni";
		String alamat2 = "Jakarta";
		String jk2 = "Pria";
		String tempatlahir2= "Kecubung Raya";
		int umur2 = 20;*/
	}
	
}

