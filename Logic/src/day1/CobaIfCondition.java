package day1;

public class CobaIfCondition
{

	public static void main(String[] args) 
	{
		int x = 15;
		if (x/3 == 5) 
		{ 
			System.out.println("Hasilnya benar");
		}
		if (x*2 == 30)
		{
			System.out.println("Hasilnya benar");
		}
		if (x % 2 == 0)
		{
			System.out.println("x % 2 => 0");
		}
		if (x/5 == x % 4)
		{
			System.out.println("x/5 => x % 4");
		}

	}

}
