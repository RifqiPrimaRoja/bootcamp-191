1. Tampilkan Data "NM_FAKULTAS","NM_JURUSAN":
SELECT T1."NM_FAKULTAS",T2."NM_JURUSAN" FROM "Fakultas" T1,"Jurusan" T2;

2. Tampilkan Data NM_FAKULTAS, NM_JURUSAN yang NM_FAKULTAS mengandung nama'fi':
SELECT T1."NM_FAKULTAS",T2."NM_JURUSAN",T2."KD_JURUSAN"
FROM "Fakultas" T1 INNER JOIN "Jurusan" T2 ON T1."KD_FAKULTAS" = T2."KD_FAKULTAS"
WHERE "NM_FAKULTAS" LIKE'%Fa%';

3.  Tampilkan DAta NM_FAKULTAS, NM_JURUSAN yang NM_JURUSAN mengandung nama'ti':
SELECT T1."NM_FAKULTAS",T2."NM_JURUSAN",T2."KD_JURUSAN"
FROM "Fakultas" T1 INNER JOIN "Jurusan" T2 ON T1."KD_FAKULTAS" = T2."KD_FAKULTAS"
WHERE "NM_JURUSAN" LIKE'%ti%';

4. Tampilkan data NM_FAKULTAS, JML_JURUSAN
SELECT t1."NM_FAKULTAS" , COUNT(t2."KD_FAKULTAS") AS "JML_JURUSAN"
FROM "Fakultas" t1
LEFT JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
GROUP BY t1."NM_FAKULTAS";

5. Tampilkan data NM_FAKULTAS, JML_JURUSAN yang JML_JURUSAN lebih dari 1
SELECT t1."NM_FAKULTAS" , COUNT(t2."KD_FAKULTAS") AS "JML_JURUSAN"
FROM "Fakultas" t1
LEFT JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
GROUP BY t1."NM_FAKULTAS"
HAVING COUNT(t2."KD_FAKULTAS")>1;

6. Tampilkan data NM_FAKULTAS, JML_JURUSAN yang tidak memiliki jurusan
SELECT t1."NM_FAKULTAS" , COUNT(t2."KD_FAKULTAS") AS "JML_JURUSAN"
FROM "Fakultas" t1
LEFT JOIN "Jurusan" t2 ON t1."KD_FAKULTAS"=t2."KD_FAKULTAS"
GROUP BY t1."NM_FAKULTAS"
HAVING COUNT(t2."KD_FAKULTAS")<1;

7. Tampilkan data NM_MK, NM_JURUSAN, NM_FAKULTAS
SELECT "NM_MK","NM_JURUSAN","NM_FAKULTAS"
FROM "MataKuliah" N1 LEFT JOIN "Jurusan" N2 ON N1."KD_JURUSAN" = N2."KD_JURUSAN" 
LEFT JOIN "Fakultas" N3 ON N2."KD_FAKULTAS" = N3."KD_FAKULTAS"
ORDER BY "NM_FAKULTAS";

8. Tampilkan data NM_JURUSAN, JML_MK
SELECT T1."NM_JURUSAN", COUNT(T2."KD_JURUSAN") AS "JML_MATKUL"
FROM "Jurusan" T1 LEFT JOIN "MataKuliah" T2 ON T1."KD_JURUSAN" = T2."KD_JURUSAN"
GROUP BY T1."NM_JURUSAN";

9. Tampilkan data NM_JURUSAN, JML_MK yang SKSnya 2
SELECT t2."NM_JURUSAN", COUNT (t3."KD_JURUSAN") AS "JML_MATKUL"
FROM "Jurusan" t2 LEFT JOIN "MataKuliah" t3 ON t2."KD_JURUSAN" = t3."KD_JURUSAN"
WHERE t3."SKS" =2
GROUP BY t2."NM_JURUSAN";

10.Tampilkan data NM_JURUSAN, JML_MK yang SKSnya 3
SELECT t2."NM_JURUSAN", COUNT(t3."KD_JURUSAN") AS "JML_MATKUL"
FROM "Jurusan" t2 LEFT JOIN "MataKuliah" t3 ON t2."KD_JURUSAN" = t3."KD_JURUSAN"
WHERE t3."SKS" = 3
GROUP BY t2."NM_JURUSAN";

11. Tampilkan data NM_FAKULTAS, NM_JURUSAN yang tidak memiliki MataKuliah
SELECT t1."NM_FAKULTAS", t2."NM_JURUSAN" 
FROM "Fakultas" t1 INNER JOIN "Jurusan" t2 ON t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
LEFT JOIN "MataKuliah" t3 ON t2."KD_JURUSAN" = t3."KD_JURUSAN"
GROUP BY "NM_FAKULTAS", "NM_JURUSAN"
HAVING COUNT(t3."KD_MK") = 0;

12. Tampilkan data NM_MK, NM_JURUSAN, NM_FAKULTAS dimana NM_MK mengandung kata 'is'
FROM "Fakultas" t1 LEFT JOIN "Jurusan" t2 ON t1."KD_FAKULTAS" = t2."KD_FAKULTAS"
LEFT JOIN "MataKuliah" t3 ON t2."KD_JURUSAN" = t3."KD_JURUSAN"
-- WHERE t3."NM_MK" LIKE '%is%'; (Bisa menggunakan "WHERE" atau "GROUP BY")
GROUP BY "NM_MK", "NM_JURUSAN", "NM_FAKULTAS"
HAVING t3."NM_MK" LIKE '%is%';

13. Tampilkan data NM_KOTA, JML_DOSEN
/*Variabel "t4" juga bisa ditiadakan"*/
SELECT t4."ALAMAT" AS "NM_KOTA", COUNT(t4."KD_DOSEN") AS "JML_DOSEN"
FROM "Dosen" t4
GROUP BY t4."ALAMAT";

14. Tampilkan data NM_DOSEN, NM_MK, NM_JURUSAN, NM_FAKULTAS, NM_KELAS, NM_RUANG
SELECT t4."NM_DOSEN", t3."NM_MK", t2."NM_JURUSAN", t1."NM_FAKULTAS", t8."NM_KELAS", t6."NM_RUANG"
FROM "Kelas" t8 JOIN "Dosen" t4 ON t8."KD_DOSEN" = t4."KD_DOSEN"
JOIN "TabelRuang" t6 ON t8."KD_RUANG" = t6."KD_RUANG"
JOIN "MataKuliah" t3 ON t8."KD_MK" = t3."KD_MK"
LEFT JOIN "Jurusan" t2 ON t3."KD_JURUSAN" = t2."KD_JURUSAN"
LEFT JOIN "Fakultas" t1 ON t2."KD_FAKULTAS" = t1."KD_FAKULTAS"
-- "GROUP BY": untuk mengelompokkan data, tanpa menggunakannya, tetap bisa jalan programnya, kecuali untuk yang menggunakan "COUNT", wajib menggunakan "GROUP BY"
GROUP BY t4."NM_DOSEN", t3."NM_MK", t2."NM_JURUSAN", t1."NM_FAKULTAS", t8."NM_KELAS", t6."NM_RUANG"
-- "ORDER BY": Hanya untuk mengurutkan kata sesuai abjad
ORDER BY "NM_JURUSAN";

15A. Tampilkan data NM_DOSEN, NM_MK, NM_JURUSAN, NM_FAKULTAS
SELECT t4."NM_DOSEN", t3."NM_MK", t2."NM_JURUSAN", t1."NM_FAKULTAS"
FROM "Kelas" t8 JOIN "Dosen" t4 ON t8."KD_DOSEN" = t4."KD_DOSEN"
JOIN "MataKuliah" t3 ON t8."KD_MK" = t3."KD_MK"
LEFT JOIN "Jurusan" t2 ON t3."KD_JURUSAN" = t2."KD_JURUSAN"
LEFT JOIN "Fakultas" t1 ON t2."KD_FAKULTAS" = t1."KD_FAKULTAS";

15B. Tampilkan data NM_DOSEN, JML_MK
SELECT t4."NM_DOSEN", COUNT (t3."KD_MK") AS "JML_MATKUL"
FROM "Kelas" t8 JOIN "Dosen" t4 ON t4."KD_DOSEN" = t8."KD_DOSEN" 
JOIN "MataKuliah" t3 ON t8."KD_MK" = t3."KD_MK"
GROUP BY "NM_DOSEN";

16. Berapa rata-rata kapasitas ruangan
-- Tanpa "t6", query tetap dapat dijalankan, karena hanya tertuju pada 1 tabel saja
SELECT AVG(t6."KAPASITAS") AS "RATARATA_KAPASITAS_RUANG"
FROM "TabelRuang" t6;

17. Berapa total mahasiswa yang bisa ditampung dalam semua ruangan
-- Perintah "SUM" berfungsi untuk menghitung value(nilai) dari total mahasiswa yg dapat ditampung dalam semua ruangan
SELECT SUM("KAPASITAS") AS "DAYA_TAMPUNG_MHS(Orang)"
FROM "TabelRuang";

18. Tampilkan ruangan yang memiliki kapasitas paling kecil

SELECT t1."NM_RUANG" AS "RUANG_KAPASITAS_MIN", t1."KAPASITAS" FROM "TabelRuang" t1 
INNER JOIN (SELECT MIN(t1."KAPASITAS") AS "MIN" FROM "TabelRuang" t1) t2 
ON t1."KAPASITAS" = t2."MIN";

19. Tampilkan ruangan yang memiliki kapasitas paling besar
-- Fungsi dari AS "RUANG_KAPASITAS_MAX" adalah untuk mengganti judul tabel dari "NM_RUANG"
SELECT t1."NM_RUANG" AS "RUANG_KAPASITAS_MAX", t1."KAPASITAS"
FROM "TabelRuang" t1
INNER JOIN (SELECT MAX(t1."KAPASITAS") AS "MAX" FROM "TabelRuang" t1) t2 
ON t1."KAPASITAS" = t2."MAX";

20. Tampilkan data NM_DOSEN, NM_MK, NM_JURUSAN, NM_FAKULTAS, NM_KELAS, NM_RUANG kapasitas ruang paling banyak
